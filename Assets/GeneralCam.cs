﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralCam : MonoBehaviour
{
    public GameObject general;
    public GameObject main;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(InputManager.main.generalCam))
        {
            general.SetActive(true);
            main.SetActive(false);
        }
    }
}
