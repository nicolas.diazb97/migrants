﻿using UnityEngine;
using System.Collections;
using UnitySocketIO;
using UnitySocketIO.Events;

public class SocketTest : Singleton<SocketTest> {

	public SocketIOController io;
    public Transform cam;
    public float fromX = -0.09914598f;
    public float fromY = -0.316477f;
    public float fromZ = -0.06261629f;
    public float toX = 0.1244674F;
    public float toY = 0.3154005f;
    public float toZ = 0.09497711f;

	void Start() {
        io.On("connect", (SocketIOEvent e) => {
            Debug.Log("SocketIO connected");


        });

        io.Connect();

        io.On("test-event", (SocketIOEvent e) => {
            Debug.Log(e.data);
        });
        StartCoroutine(SendData());
    }
    public void Update()
    {
        Debug.LogError("x: " + cam.rotation.x);
        Debug.LogError("y: " + cam.rotation.y);
        Debug.LogError("z: " + cam.rotation.z);
    }
    IEnumerator SendData()
    {
        TestObject t = new TestObject();
        t.x = cam.rotation.x.Remap(fromX, toX, 0, 127).ToString();
        t.y = cam.rotation.y.Remap(fromX, toX, 0, 127).ToString();
        t.z = cam.rotation.z.Remap(fromX, toX, 0, 127).ToString();

        io.Emit("test-event2", JsonUtility.ToJson(t));
        yield return new WaitForSecondsRealtime(0.2f);
        StartCoroutine(SendData());
    }

}
public static class ExtensionMethods
{

    public static float Remap(this float value, float from1, float to1, float from2, float to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

}
