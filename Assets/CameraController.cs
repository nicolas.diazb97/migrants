﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject timeLine;
    public GameObject generalTimeLine;
    bool onGeneral = false;
    KeyCode mainKey;
    // Start is called before the first frame update
    void Start()
    {
        mainKey = InputManager.main.generalCam;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(mainKey))
        {
            if (onGeneral)
            {
                timeLine.SetActive(true);
                generalTimeLine.SetActive(false);
                onGeneral = false;
            }
            else
            {
                timeLine.SetActive(false);
                generalTimeLine.SetActive(true);
                onGeneral = true;
            }
        }
    }
}
