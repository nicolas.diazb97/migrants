// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "DCEP_Stream_SD"
{
	Properties
	{
		_Color("Color", Color) = (0,0,0,0)
		_AlbedoMap("AlbedoMap", 2D) = "white" {}
		_Ripples("Ripples", 2D) = "bump" {}
		_Ripples2("Ripples2", 2D) = "bump" {}
		_Ripples_Displacement("Ripples_Displacement", 2D) = "gray" {}
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		_Metallic("Metallic", Range( 0 , 1)) = 0
		_Edge_Darkening("Edge_Darkening", Range( 0 , 1)) = 0
		_RipplesScale("Ripples Scale", Range( 0 , 1)) = 0
		_Ripples2Scale("Ripples2 Scale", Range( 0 , 1)) = 0
		_RipplesSpeed("Ripples Speed", Vector) = (1,0,0,0)
		_Ripples2Speed("Ripples2 Speed", Vector) = (2.5,0,0,0)
		_Displacement("Displacement", Range( 0 , 10)) = 0
		_DisplacementOffset("Displacement Offset", Range( -5 , 5)) = 0
		_Transparency("Transparency", Range( 0 , 1)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Back
		CGINCLUDE
		#include "UnityShaderVariables.cginc"
		#include "UnityStandardUtils.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
		};

		uniform sampler2D _Ripples_Displacement;
		uniform float2 _Ripples2Speed;
		uniform sampler2D _Ripples2;
		uniform float4 _Ripples2_ST;
		uniform float _Displacement;
		uniform float _DisplacementOffset;
		uniform sampler2D _Ripples;
		uniform float2 _RipplesSpeed;
		uniform float4 _Ripples_ST;
		uniform float _RipplesScale;
		uniform float _Ripples2Scale;
		uniform sampler2D _AlbedoMap;
		uniform float4 _Color;
		uniform float _Edge_Darkening;
		uniform float _Metallic;
		uniform float _Smoothness;
		uniform float _Transparency;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float2 uv_Ripples2 = v.texcoord.xy * _Ripples2_ST.xy + _Ripples2_ST.zw;
			float2 panner14 = ( _Time.x * _Ripples2Speed + uv_Ripples2);
			float3 ase_vertexNormal = v.normal.xyz;
			float4 temp_cast_1 = (_DisplacementOffset).xxxx;
			v.vertex.xyz += ( ( tex2Dlod( _Ripples_Displacement, float4( panner14, 0, 0.0) ) * float4( ( ase_vertexNormal * _Displacement ) , 0.0 ) ) - temp_cast_1 ).rgb;
		}

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Ripples = i.uv_texcoord * _Ripples_ST.xy + _Ripples_ST.zw;
			float2 panner13 = ( _Time.x * _RipplesSpeed + uv_Ripples);
			float2 uv_Ripples2 = i.uv_texcoord * _Ripples2_ST.xy + _Ripples2_ST.zw;
			float2 panner14 = ( _Time.x * _Ripples2Speed + uv_Ripples2);
			o.Normal = ( UnpackScaleNormal( tex2D( _Ripples, panner13 ), ( _RipplesScale * 1 ) ) + UnpackScaleNormal( tex2D( _Ripples2, panner14 ), ( _Ripples2Scale * 1 ) ) );
			float4 lerpResult47 = lerp( ( tex2D( _AlbedoMap, panner13 ) * _Color ) , i.vertexColor , _Edge_Darkening);
			o.Albedo = lerpResult47.rgb;
			o.Metallic = _Metallic;
			o.Smoothness = _Smoothness;
			o.Alpha = ( lerpResult47 * _Transparency ).r;
		}

		ENDCG
		CGPROGRAM
		#pragma surface surf Standard alpha:fade keepalpha fullforwardshadows exclude_path:deferred vertex:vertexDataFunc 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			sampler3D _DitherMaskLOD;
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float2 customPack1 : TEXCOORD1;
				float3 worldPos : TEXCOORD2;
				float4 tSpace0 : TEXCOORD3;
				float4 tSpace1 : TEXCOORD4;
				float4 tSpace2 : TEXCOORD5;
				half4 color : COLOR0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				vertexDataFunc( v, customInputData );
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.worldPos = worldPos;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.color = v.color;
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				float3 worldPos = IN.worldPos;
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.vertexColor = IN.color;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				half alphaRef = tex3D( _DitherMaskLOD, float3( vpos.xy * 0.25, o.Alpha * 0.9375 ) ).a;
				clip( alphaRef - 0.01 );
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18400
0;72.66667;2016;770;3941.096;764.1661;2.367816;True;True
Node;AmplifyShaderEditor.TimeNode;12;-2637.163,392.081;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;56;-2838.418,185.4523;Inherit;False;Property;_RipplesSpeed;Ripples Speed;12;0;Create;True;0;0;False;0;False;1,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;54;-2617.138,22.11742;Inherit;False;0;26;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;13;-2288.91,174.9315;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-10,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;55;-2639.518,554.6525;Inherit;False;0;23;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;57;-2869.619,480.5523;Inherit;False;Property;_Ripples2Speed;Ripples2 Speed;13;0;Create;True;0;0;False;0;False;2.5,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.NormalVertexDataNode;16;-1356.224,715.7089;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;15;-1321.197,887.415;Float;False;Property;_Displacement;Displacement;14;0;Create;True;0;0;False;0;False;0;0.3;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;51;-2383.867,-166.8896;Inherit;False;Property;_RipplesScale;Ripples Scale;10;0;Create;True;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;14;-2274.025,345.2787;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-25,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;52;-2601.659,767.8477;Inherit;False;Property;_Ripples2Scale;Ripples2 Scale;11;0;Create;True;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;17;-1733.629,-507.8099;Inherit;True;Property;_AlbedoMap;AlbedoMap;1;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;18;-1316.73,-354.4315;Float;False;Property;_Color;Color;0;0;Create;True;0;0;False;0;False;0,0,0,0;0.4533197,0.5754716,0.5076095,1;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;22;-1984.123,556.5548;Inherit;True;Property;_Ripples_Displacement;Ripples_Displacement;4;0;Create;True;0;0;False;0;False;-1;None;None;True;0;False;gray;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ScaleNode;50;-2206.851,526.4929;Inherit;False;1;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;20;-814.2775,-361.9529;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;21;-1007.733,-438.2659;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScaleNode;53;-2165.88,-41.70812;Inherit;False;1;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-565.4039,-291.6351;Inherit;False;Property;_Edge_Darkening;Edge_Darkening;9;0;Create;True;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;-904.655,671.5914;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;66;-563.1326,-197.2583;Inherit;False;Property;_Transparency;Transparency;17;0;Create;True;0;0;False;0;False;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;59;-749.6172,763.3892;Inherit;False;Property;_DisplacementOffset;Displacement Offset;15;0;Create;True;0;0;False;0;False;0;0;-5;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;23;-1989.125,270.6585;Inherit;True;Property;_Ripples2;Ripples2;3;0;Create;True;0;0;False;0;False;-1;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;47;-256.0123,-435.7949;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;26;-1914.917,36.12367;Inherit;True;Property;_Ripples;Ripples;2;0;Create;True;0;0;False;0;False;-1;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-501.6496,486.5585;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;67;-159.3882,-252.8611;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;28;-2664.906,277.4421;Float;False;Property;_Speed2;Speed 2;8;0;Create;True;0;0;False;0;False;0;0;-10;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;58;-282.3874,539.3842;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;41;-1474.744,110.7859;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;35;-2640.49,157.1806;Float;False;Property;_Speed;Speed;7;0;Create;True;0;0;False;0;False;0;-3;-10;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25;-317.2088,123.7097;Float;False;Property;_Smoothness;Smoothness;5;0;Create;True;0;0;False;0;False;0;0.98;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;46;-319.1495,44.96444;Float;False;Property;_Metallic;Metallic;6;0;Create;True;0;0;False;0;False;0;0.98;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;65;-287.6584,-133.2665;Inherit;False;Property;_Color0;Color 0;16;0;Create;True;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;68;216.4808,-19.92124;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;DCEP_Stream_SD;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Transparent;0.5;True;True;0;False;Transparent;;Transparent;ForwardOnly;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;True;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;2;0;0;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;True;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;13;0;54;0
WireConnection;13;2;56;0
WireConnection;13;1;12;1
WireConnection;14;0;55;0
WireConnection;14;2;57;0
WireConnection;14;1;12;1
WireConnection;17;1;13;0
WireConnection;22;1;14;0
WireConnection;50;0;52;0
WireConnection;21;0;17;0
WireConnection;21;1;18;0
WireConnection;53;0;51;0
WireConnection;19;0;16;0
WireConnection;19;1;15;0
WireConnection;23;1;14;0
WireConnection;23;5;50;0
WireConnection;47;0;21;0
WireConnection;47;1;20;0
WireConnection;47;2;48;0
WireConnection;26;1;13;0
WireConnection;26;5;53;0
WireConnection;34;0;22;0
WireConnection;34;1;19;0
WireConnection;67;0;47;0
WireConnection;67;1;66;0
WireConnection;58;0;34;0
WireConnection;58;1;59;0
WireConnection;41;0;26;0
WireConnection;41;1;23;0
WireConnection;68;0;47;0
WireConnection;68;1;41;0
WireConnection;68;3;46;0
WireConnection;68;4;25;0
WireConnection;68;9;67;0
WireConnection;68;11;58;0
ASEEND*/
//CHKSM=7F1DCC2AE6DD6BDD3056615EBE8F307E72A0191A