﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PreviousScene : MonoBehaviour
{
    public string previous;
    KeyCode mainKey;
    // Start is called before the first frame update
    void Start()
    {
        mainKey = InputManager.main.prevScene;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(mainKey))
        {
            SceneManager.LoadScene(previous);
        }
    }
}
