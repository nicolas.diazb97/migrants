﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAroundIt : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    int _rotationSpeed = 15;

    void Update()
    {

        // Rotation on y axis
        transform.Rotate(0, _rotationSpeed * Time.deltaTime, 0);
    }
}
