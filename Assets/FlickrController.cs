﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickrController : MonoBehaviour
{
    public GameObject flicker;
    KeyCode mainKey;
    // Start is called before the first frame update
    void Start()
    {
        mainKey = InputManager.main.flickr;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(mainKey))
        {
            flicker.SetActive(true);
        }
        if (Input.GetKeyUp(mainKey))
        {
            flicker.SetActive(false);
        }
    }
}
