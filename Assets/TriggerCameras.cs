﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCameras : MonoBehaviour
{
    public Cinemachine.CinemachineVirtualCamera camera;
    public Transform target;
  
    private void OnTriggerEnter(Collider other)
    {
        camera.m_LookAt = target;
    }
    private void OnTriggerExit(Collider other)
    {
        camera.m_LookAt = null;
    }
}
