﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenFade : MonoBehaviour
{

    [Header("initial fade")]
    public Color from;
    public Color to;

    [Header("final fade")]
    public Color from2;
    public Color to2;
    public bool once = true;
    public float finalTime;
    public string sceneName;
    KeyCode mainKey;

    // Start is called before the first frame update
    void Start()
    {
        mainKey = InputManager.main.changeScene;
        StartCoroutine(WaitForStart()); 
        Cursor.visible = false;
    }
    IEnumerator WaitForStart()
    {
        yield return new WaitForSecondsRealtime(0f);
        FadeManager.main.StartFade(7f, from, to);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(mainKey))
        {
            Debug.LogError("cambio de escena");
            FadeManager.main.Fade(finalTime, from2, to2, this);
            once = false;
        }
    }
    public void CallbackFade()
    {
        Debug.LogError("callback");
        StartCoroutine(WaitToChangeScene(finalTime));
    }
    IEnumerator WaitToChangeScene(float _seconds)
    {
        yield return new WaitForSecondsRealtime(_seconds);
        SceneManager.LoadScene(sceneName);
    }
}
