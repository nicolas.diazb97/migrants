﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControlMouse : MonoBehaviour
{
    [Flags]
    public enum RotationDirection
    {
        None,
        Horizontal = (1 << 0),
        Vertical = (1 << 1)
    }
    [SerializeField] private RotationDirection rotationDirections;
    [SerializeField] private Vector2 acceleration;
    [SerializeField] private Vector2 sensitivity;
    [SerializeField] private float maxVerticalAngleFromHorizon;
    [SerializeField] private float inputLagPeriod;
    private Vector2 velocity;
    private Vector2 rotation;
    private Vector2 lastInputEvent;
    private float inputLagTimer;


    private void OnEnable()
    {
        velocity = Vector2.zero;
        lastInputEvent = Vector2.zero;
        Vector3 euler = transform.localEulerAngles;
        if (euler.x >= 180)
        {
            euler.x -= 360;
        }
        euler.x = ClampVerticalAngle(euler.x);
        transform.localEulerAngles = euler;
        rotation = new Vector2(euler.y, euler.x);
    }
    public Vector2 GetInput()
    {
        inputLagTimer += Time.deltaTime;
        // Get the input vector. This can be changed to work with the new input system or even touch controls
        Vector2 input = new Vector2(
            Input.GetAxis("Mouse X"),
            Input.GetAxis("Mouse Y")
        );
        if ((Mathf.Approximately(0, input.x) && Mathf.Approximately(0, input.y)) == false || inputLagTimer >= inputLagPeriod)
        {
            lastInputEvent = input;
            inputLagTimer = 0;
        }
        return lastInputEvent;
    }
    //public GameObject Controlador;
    public float sensivilidad;
    private Vector3 offset;

    private float yaw = 0.0f;
    private float pitch = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        //offset = transform.position - Controlador.transform.position;
    }
    private void LateUpdate()
    {
        //transform.position = Controlador.transform.position + offset;
    }
    private float ClampVerticalAngle(float angle)
    {
        return Mathf.Clamp(angle, -maxVerticalAngleFromHorizon, maxVerticalAngleFromHorizon);
    }
    // Update is called once per frame
    void Update()
    {
        /*float rotateHorizontal = Input.GetAxis("Mouse X");
        float rotateVertical = Input.GetAxis("Mouse Y");
        transform.Rotate(transform.right * rotateHorizontal * sensivilidad*Time.deltaTime);
        transform.Rotate(transform.up * rotateVertical * sensivilidad*Time.deltaTime);*/

        /*yaw += sensivilidad * Input.GetAxis("Mouse X");
        pitch -= sensivilidad * Input.GetAxis("Mouse Y");

        transform.eulerAngles = new Vector3(pitch, -yaw, 0.0f);*/
        if (Input.GetMouseButton(1))
        {

            Vector2 wantedVelocity = GetInput() * sensitivity;
            if ((rotationDirections & RotationDirection.Horizontal) == 0)
            {
                wantedVelocity.x = 0;
            }
            if ((rotationDirections & RotationDirection.Vertical) == 0)
            {
                wantedVelocity.y = 0;
            }
            velocity = new Vector2(
                Mathf.MoveTowards(velocity.x, wantedVelocity.x, acceleration.x * Time.deltaTime),
                Mathf.MoveTowards(velocity.y, wantedVelocity.y, acceleration.y * Time.deltaTime));
            rotation += velocity * Time.deltaTime;
            rotation.y = ClampVerticalAngle(rotation.y);
            transform.localEulerAngles = new Vector3(rotation.y, rotation.x, 0);
        }
    }
}
