﻿using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class ManagerDeCamaras : MonoBehaviour
{
    private Cinemachine.CinemachineVirtualCamera camera;
    //public List<Transform> Targets;
    //public Transform Target;
    private Vector3 MovimientoTarget;
    private bool estado;
    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Cinemachine.CinemachineVirtualCamera>();
        estado = false;
    }

    // Update is called once per frame
    void Update()
    {
        /* if (Input.GetKey(KeyCode.W)) {
             //camera.m_LookAt=(Targets[0]);
             MovimientoTarget=new Vector3(0,0.2f,0);
             Target.localPosition += MovimientoTarget;
         }else if (Input.GetKey(KeyCode.A)) {
             //camera.m_LookAt=(Targets[1]);
             MovimientoTarget = new Vector3(-0.2f, 0, 0);
             Target.localPosition += MovimientoTarget;
         }
         else if (Input.GetKey(KeyCode.S)) {
             //camera.m_LookAt=(Targets[2]);
             MovimientoTarget = new Vector3(0,-0.2f, 0);
             Target.localPosition += MovimientoTarget;
         }
         else if (Input.GetKey(KeyCode.D)) {
             //camera.m_LookAt=(Targets[3]);
             MovimientoTarget = new Vector3(0.2f, 0, 0);
             Target.localPosition += MovimientoTarget;
         }*/

        if (Input.GetKeyDown(KeyCode.H))
        {
            if (estado.Equals(true))
            {
                camera.AddCinemachineComponent<CinemachinePOV>();
                estado = false;
            }else if (estado.Equals(false))
            {
                camera.DestroyCinemachineComponent<CinemachinePOV>();
                estado = true;
            }
            
        }
    }
}
