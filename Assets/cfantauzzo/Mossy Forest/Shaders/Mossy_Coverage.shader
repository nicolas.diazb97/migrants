// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "cfantauzzo/Mossy Forest/Mossy Coverage"
{
	Properties
	{
		_Color("Color", Color) = (0,0,0,0)
		_Albedo_Base("Albedo_Base", 2D) = "white" {}
		_Normal_Base("Normal_Base", 2D) = "bump" {}
		_MaskMap("MaskMap", 2D) = "white" {}
		_CoverageMask("Coverage Mask", 2D) = "white" {}
		_MossColor("Moss Color", Color) = (0.8470588,1,0.2666667,0)
		_MossDetailmap("Moss Detailmap", 2D) = "white" {}
		[KeywordEnum(UV0,UV1)] _DetailUVMap("Detail UV Map", Float) = 0
		_DetailMap("DetailMap", 2D) = "gray" {}
		_BaseSmoothness("Base Smoothness", Range( 0 , 1)) = 1
		_DetailSmoothness("Detail Smoothness", Range( 0 , 1)) = 0
		_DetailNormalScale("Detail Normal Scale", Range( 0 , 2)) = 1
		_DetailAlbedo("Detail Albedo", Range( 0 , 2)) = 1
		_CoverageMaskTiling("Coverage Mask Tiling", Range( 0 , 10)) = 1
		_MetallicStrength("Metallic Strength", Range( 0 , 1)) = 0
		_OcclusionStrength("Occlusion Strength", Range( 0 , 1)) = 1
		_CoverageRemap2("Coverage Remap 2", Vector) = (0,1,0,1)
		_CoverageDirection("Coverage Direction", Vector) = (1,1,0,0)
		_CoverageOcclusionMask("Coverage Occlusion Mask", Range( 0 , 1)) = 1
		_CoverageNormalInfluence("Coverage Normal Influence", Range( 0 , 1)) = 1
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 3.0
		#pragma shader_feature_local _DETAILUVMAP_UV0 _DETAILUVMAP_UV1
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float2 uv2_texcoord2;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform sampler2D _Normal_Base;
		uniform float4 _Normal_Base_ST;
		uniform sampler2D _DetailMap;
		uniform float4 _DetailMap_ST;
		uniform float _DetailNormalScale;
		uniform sampler2D _MaskMap;
		uniform float4 _MaskMap_ST;
		uniform sampler2D _MossDetailmap;
		uniform float _CoverageNormalInfluence;
		uniform float3 _CoverageDirection;
		uniform float4 _CoverageRemap2;
		sampler2D _CoverageMask;
		uniform float _CoverageMaskTiling;
		uniform float _CoverageOcclusionMask;
		uniform float _DetailAlbedo;
		uniform sampler2D _Albedo_Base;
		uniform float4 _Albedo_Base_ST;
		uniform float4 _Color;
		uniform float4 _MossColor;
		uniform float _MetallicStrength;
		uniform float _BaseSmoothness;
		uniform float _DetailSmoothness;
		uniform float _OcclusionStrength;


		inline float4 TriplanarSampling46( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = tex2D( topTexMap, tiling * worldPos.zy * float2(  nsign.x, 1.0 ) );
			yNorm = tex2D( topTexMap, tiling * worldPos.xz * float2(  nsign.y, 1.0 ) );
			zNorm = tex2D( topTexMap, tiling * worldPos.xy * float2( -nsign.z, 1.0 ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		inline float4 TriplanarSampling47( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = tex2D( topTexMap, tiling * worldPos.zy * float2(  nsign.x, 1.0 ) );
			yNorm = tex2D( topTexMap, tiling * worldPos.xz * float2(  nsign.y, 1.0 ) );
			zNorm = tex2D( topTexMap, tiling * worldPos.xy * float2( -nsign.z, 1.0 ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal_Base = i.uv_texcoord * _Normal_Base_ST.xy + _Normal_Base_ST.zw;
			float3 tex2DNode2 = UnpackNormal( tex2D( _Normal_Base, uv_Normal_Base ) );
			float2 uv_DetailMap = i.uv_texcoord * _DetailMap_ST.xy + _DetailMap_ST.zw;
			float2 uv2_DetailMap = i.uv2_texcoord2 * _DetailMap_ST.xy + _DetailMap_ST.zw;
			#if defined(_DETAILUVMAP_UV0)
				float2 staticSwitch131 = uv_DetailMap;
			#elif defined(_DETAILUVMAP_UV1)
				float2 staticSwitch131 = uv2_DetailMap;
			#else
				float2 staticSwitch131 = uv_DetailMap;
			#endif
			float4 tex2DNode110 = tex2D( _DetailMap, staticSwitch131 );
			float4 appendResult114 = (float4(tex2DNode110.a , tex2DNode110.g , 1.0 , 1.0));
			float2 uv_MaskMap = i.uv_texcoord * _MaskMap_ST.xy + _MaskMap_ST.zw;
			float4 tex2DNode3 = tex2D( _MaskMap, uv_MaskMap );
			float3 lerpResult174 = lerp( float3( 0,0,1 ) , UnpackScaleNormal( appendResult114, _DetailNormalScale ) , tex2DNode3.b);
			float3 lerpResult117 = lerp( tex2DNode2 , BlendNormals( tex2DNode2 , lerpResult174 ) , tex2DNode3.b);
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float4 triplanar46 = TriplanarSampling46( _MossDetailmap, ase_vertex3Pos, ase_vertexNormal, 1.0, float2( 1,1 ), 1.0, 0 );
			float4 appendResult63 = (float4(triplanar46.w , triplanar46.y , 1.0 , 1.0));
			float3 lerpResult196 = lerp( float3( 0,0,1 ) , lerpResult117 , _CoverageNormalInfluence);
			float3 newWorldNormal4 = (WorldNormalVector( i , lerpResult196 ));
			float2 appendResult91 = (float2(_CoverageMaskTiling , _CoverageMaskTiling));
			float4 triplanar47 = TriplanarSampling47( _CoverageMask, ase_worldPos, ase_worldNormal, 1.0, appendResult91, 1.0, 0 );
			float4 CoverageMaskVar175 = ( ( saturate( (_CoverageRemap2.z + (( ( newWorldNormal4.x * _CoverageDirection.x ) + ( newWorldNormal4.y * _CoverageDirection.y ) + ( newWorldNormal4.z * _CoverageDirection.z ) ) - _CoverageRemap2.x) * (_CoverageRemap2.w - _CoverageRemap2.z) / (_CoverageRemap2.y - _CoverageRemap2.x)) ) * triplanar47 ) * saturate( (0.0 + (tex2DNode3.g - _CoverageOcclusionMask) * (1.0 - 0.0) / (1.0 - _CoverageOcclusionMask)) ) );
			float3 lerpResult18 = lerp( lerpResult117 , BlendNormals( lerpResult117 , UnpackScaleNormal( appendResult63, 0.9 ) ) , CoverageMaskVar175.xyz);
			float3 NormalVarOutput160 = lerpResult18;
			o.Normal = NormalVarOutput160;
			float4 DetailMapVar137 = tex2DNode110;
			float saferPower113 = max( ( DetailMapVar137.r * 2.0 ) , 0.0001 );
			float4 MaskMapVar143 = tex2DNode3;
			float lerpResult121 = lerp( 0.5 , pow( saferPower113 , _DetailAlbedo ) , MaskMapVar143.b);
			float4 temp_cast_3 = (lerpResult121).xxxx;
			float2 uv_Albedo_Base = i.uv_texcoord * _Albedo_Base_ST.xy + _Albedo_Base_ST.zw;
			float4 blendOpSrc122 = temp_cast_3;
			float4 blendOpDest122 = ( tex2D( _Albedo_Base, uv_Albedo_Base ) * _Color );
			float4 lerpResult7 = lerp( ( saturate( (( blendOpDest122 > 0.5 ) ? ( 1.0 - 2.0 * ( 1.0 - blendOpDest122 ) * ( 1.0 - blendOpSrc122 ) ) : ( 2.0 * blendOpDest122 * blendOpSrc122 ) ) )) , ( _MossColor * triplanar46.x ) , CoverageMaskVar175);
			float4 AlbedoVarOutput163 = lerpResult7;
			o.Albedo = AlbedoVarOutput163.rgb;
			float MetallicVarOutput148 = ( MaskMapVar143.r * _MetallicStrength );
			o.Metallic = MetallicVarOutput148;
			float lerpResult169 = lerp( 0.5 , DetailMapVar137.b , _DetailSmoothness);
			float blendOpSrc136 = ( tex2DNode3.a * _BaseSmoothness );
			float blendOpDest136 = lerpResult169;
			float lerpResult17 = lerp( ( saturate( (( blendOpDest136 > 0.5 ) ? ( 1.0 - 2.0 * ( 1.0 - blendOpDest136 ) * ( 1.0 - blendOpSrc136 ) ) : ( 2.0 * blendOpDest136 * blendOpSrc136 ) ) )) , triplanar46.z , CoverageMaskVar175.x);
			float SmoothnessVarOutput165 = saturate( lerpResult17 );
			o.Smoothness = SmoothnessVarOutput165;
			float lerpResult156 = lerp( 1.0 , MaskMapVar143.g , _OcclusionStrength);
			float OcclusionVarOutput158 = lerpResult156;
			o.Occlusion = OcclusionVarOutput158;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma exclude_renderers xboxseries playstation 
		#pragma surface surf Standard keepalpha fullforwardshadows dithercrossfade 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.customPack1.zw = customInputData.uv2_texcoord2;
				o.customPack1.zw = v.texcoord1;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				surfIN.uv2_texcoord2 = IN.customPack1.zw;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18909
22.66667;186.6667;1920;529;2571.585;244.4551;1.815453;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;132;-3127.256,422.6827;Inherit;False;0;110;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;135;-3126.856,558.2808;Inherit;False;1;110;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;131;-2849.997,478.7485;Inherit;False;Property;_DetailUVMap;Detail UV Map;7;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;2;UV0;UV1;Create;True;True;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;110;-2628.557,454.0101;Inherit;True;Property;_DetailMap;DetailMap;8;0;Create;True;0;0;0;False;0;False;-1;None;fe90b40457d24014a9085b1f8e9cc909;True;1;False;gray;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;115;-1808.296,799.5497;Inherit;False;Property;_DetailNormalScale;Detail Normal Scale;11;0;Create;True;0;0;0;False;0;False;1;0.6;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;114;-1746.077,557.5446;Inherit;True;COLOR;4;0;FLOAT;1;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.UnpackScaleNormalNode;116;-1469.071,520.3776;Inherit;True;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;3;-1520.238,873.988;Inherit;True;Property;_MaskMap;MaskMap;3;0;Create;True;0;0;0;False;0;False;-1;None;3704d65739729a8408d34f4ee524bfa8;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;-3403.562,159.6773;Inherit;True;Property;_Normal_Base;Normal_Base;2;0;Create;True;0;0;0;False;0;False;-1;None;8323150be61acdd42ad1e4744d528bbb;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;174;-1147.447,538.8942;Inherit;True;3;0;FLOAT3;0,0,1;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BlendNormalsNode;129;-867.2092,487.88;Inherit;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;117;-566.177,434.528;Inherit;True;3;0;FLOAT3;0,0,1;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;194;-3376,-160;Inherit;False;Property;_CoverageNormalInfluence;Coverage Normal Influence;19;0;Create;True;0;0;0;False;0;False;1;0.7;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;173;-2789.758,-395.9013;Inherit;False;1916.861;584.9668;;18;191;175;187;9;42;47;91;188;85;179;181;186;182;4;192;193;48;190;Coverage Mask;1,1,1,1;0;0
Node;AmplifyShaderEditor.LerpOp;196;-3040,-144;Inherit;True;3;0;FLOAT3;0,0,1;False;1;FLOAT3;0,0,1;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector3Node;182;-2720,-160;Inherit;False;Property;_CoverageDirection;Coverage Direction;17;0;Create;True;0;0;0;False;0;False;1,1,0;0.3,3,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;4;-2752,-336;Inherit;False;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;186;-2432,-272;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;193;-2432,-176;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;192;-2432,-352;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;85;-2592.185,68.55412;Inherit;False;Property;_CoverageMaskTiling;Coverage Mask Tiling;13;0;Create;True;0;0;0;False;0;False;1;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;181;-2303.222,-162.7183;Inherit;False;Property;_CoverageRemap2;Coverage Remap 2;16;0;Create;True;0;0;0;False;0;False;0,1,0,1;0.5,1,0,1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;48;-2208,-336;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;137;-2239.133,454.0459;Inherit;False;DetailMapVar;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;172;-1808.892,-1675.125;Inherit;False;2447.57;1008.279;;17;178;163;7;59;122;60;121;44;113;145;1;43;112;144;111;142;141;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;91;-2276.301,45.37687;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TFHCRemapNode;179;-1979.314,-280.5789;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;188;-1688.605,48.83804;Inherit;False;Property;_CoverageOcclusionMask;Coverage Occlusion Mask;18;0;Create;True;0;0;0;False;0;False;1;0.85;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;170;-1219.94,1236.063;Inherit;False;1061.124;522.804;;7;139;140;166;119;168;169;136;Smoothness;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;141;-1758.892,-1625.125;Inherit;False;137;DetailMapVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.TriplanarNode;47;-2072.061,-14.75333;Inherit;True;Spherical;World;False;Coverage Mask;_CoverageMask;white;4;None;Mid Texture 1;_MidTexture1;white;11;None;Bot Texture 1;_BotTexture1;white;12;None;Coverage Mask;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;190;-1369.026,-47.02846;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;-10;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;42;-1571.268,-298.1709;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;139;-1081.656,1494.845;Inherit;False;137;DetailMapVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-1408.149,-248.0839;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;143;-1049.47,879.3346;Inherit;False;MaskMapVar;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;191;-1093.427,-48.32841;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;142;-1538.202,-1620.747;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.TexturePropertyNode;61;-1428.181,-621.1656;Inherit;True;Property;_MossDetailmap;Moss Detailmap;6;0;Create;True;0;0;0;False;0;False;843c111985eec614a981078bb54447fd;843c111985eec614a981078bb54447fd;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;187;-1259.09,-239.0195;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TriplanarNode;46;-1186.638,-616.0929;Inherit;True;Spherical;Object;False;Albedo_Cover;_Albedo_Cover;white;4;None;Mid Texture 3;_MidTexture3;white;11;None;Bot Texture 3;_BotTexture3;white;12;None;Albedo_Cover;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;111;-1368.676,-1622.068;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;166;-994.0314,1638.932;Inherit;False;Property;_DetailSmoothness;Detail Smoothness;10;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;140;-850.0244,1479.372;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;112;-1446.719,-1355.433;Inherit;False;Property;_DetailAlbedo;Detail Albedo;12;0;Create;True;0;0;0;False;0;False;1;1;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;144;-1091.869,-1318.308;Inherit;False;143;MaskMapVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;119;-1169.94,1392.896;Inherit;False;Property;_BaseSmoothness;Base Smoothness;9;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;157;-1210.767,1853.387;Inherit;False;966.1534;334.8259;;5;158;151;152;154;156;Occlusion;1,1,1,1;0;0
Node;AmplifyShaderEditor.BreakToComponentsNode;145;-898.1427,-1312.818;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SamplerNode;1;-918.4742,-1146.847;Inherit;True;Property;_Albedo_Base;Albedo_Base;1;0;Create;True;0;0;0;False;0;False;-1;None;dd055d4d2290bb34698a1affe5aa3cec;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;150;-1190.174,2350.405;Inherit;False;902.8506;363.1353;;5;107;106;146;147;148;Metallic;1,1,1,1;0;0
Node;AmplifyShaderEditor.LerpOp;169;-701.9284,1511.873;Inherit;True;3;0;FLOAT;0.5;False;1;FLOAT;0.5;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;63;-418.5961,243.5986;Inherit;False;COLOR;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;168;-724.1279,1294.471;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;175;-1110.233,-237.0302;Inherit;False;CoverageMaskVar;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.PowerNode;113;-1074.251,-1579.145;Inherit;True;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;43;-862.2156,-936.0647;Float;False;Property;_Color;Color;0;0;Create;True;0;0;0;False;0;False;0,0,0,0;1,1,1,1;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;176;-99.50653,682.0158;Inherit;False;175;CoverageMaskVar;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;121;-666.3404,-1403.363;Inherit;True;3;0;FLOAT;0.5;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;151;-1160.767,1903.878;Inherit;False;143;MaskMapVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;60;-547.3649,-1011.029;Inherit;False;Property;_MossColor;Moss Color;5;0;Create;True;0;0;0;False;0;False;0.8470588,1,0.2666667,0;0.8470588,1,0.2666665,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.UnpackScaleNormalNode;64;-246.029,245.4505;Inherit;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0.9;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WireNode;171;-563.808,-650.0893;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;136;-422.3346,1436.208;Inherit;True;Overlay;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;146;-1146.458,2410.51;Inherit;False;143;MaskMapVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;-519.707,-1139.18;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;152;-947.1719,1903.387;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;154;-1092.015,2069.612;Inherit;False;Property;_OcclusionStrength;Occlusion Strength;15;0;Create;True;0;0;0;False;0;False;1;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;178;-225.8339,-767.5579;Inherit;False;175;CoverageMaskVar;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.LerpOp;17;267.8219,630.7441;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;177;213.6901,516.0667;Inherit;False;175;CoverageMaskVar;1;0;OBJECT;;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;107;-1021.691,2592.258;Inherit;False;Property;_MetallicStrength;Metallic Strength;14;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;65;130.2747,400.9735;Inherit;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;-221.7017,-1004.187;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;122;-302.9386,-1288.231;Inherit;True;Overlay;True;3;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;147;-927.8811,2400.405;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;106;-677.3778,2468.436;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;45;481.1111,655.5397;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;7;110.6831,-915.639;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;18;469.8683,260.8619;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;156;-736.1217,1921.695;Inherit;True;3;0;FLOAT;1;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;165;677.5251,644.5402;Inherit;False;SmoothnessVarOutput;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;158;-488.4471,1917.973;Inherit;False;OcclusionVarOutput;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;160;806.3941,301.9422;Inherit;True;NormalVarOutput;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;148;-532.6571,2469.592;Inherit;False;MetallicVarOutput;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;163;399.6783,-887.9279;Inherit;False;AlbedoVarOutput;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;161;1618.593,-3.060024;Inherit;False;160;NormalVarOutput;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;159;1610.031,225.3314;Inherit;False;158;OcclusionVarOutput;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;164;1604.471,152.8283;Inherit;False;165;SmoothnessVarOutput;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;149;1613.357,79.29752;Inherit;False;148;MetallicVarOutput;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;162;1614.645,-89.70665;Inherit;False;163;AlbedoVarOutput;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1907.888,10.39832;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;cfantauzzo/Mossy Forest/Mossy Coverage;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;d3d9;d3d11_9x;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;ps4;psp2;n3ds;wiiu;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;3;20;2;8;True;0.3;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;1;False;-1;1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;131;1;132;0
WireConnection;131;0;135;0
WireConnection;110;1;131;0
WireConnection;114;0;110;4
WireConnection;114;1;110;2
WireConnection;116;0;114;0
WireConnection;116;1;115;0
WireConnection;174;1;116;0
WireConnection;174;2;3;3
WireConnection;129;0;2;0
WireConnection;129;1;174;0
WireConnection;117;0;2;0
WireConnection;117;1;129;0
WireConnection;117;2;3;3
WireConnection;196;1;117;0
WireConnection;196;2;194;0
WireConnection;4;0;196;0
WireConnection;186;0;4;2
WireConnection;186;1;182;2
WireConnection;193;0;4;3
WireConnection;193;1;182;3
WireConnection;192;0;4;1
WireConnection;192;1;182;1
WireConnection;48;0;192;0
WireConnection;48;1;186;0
WireConnection;48;2;193;0
WireConnection;137;0;110;0
WireConnection;91;0;85;0
WireConnection;91;1;85;0
WireConnection;179;0;48;0
WireConnection;179;1;181;1
WireConnection;179;2;181;2
WireConnection;179;3;181;3
WireConnection;179;4;181;4
WireConnection;47;3;91;0
WireConnection;190;0;3;2
WireConnection;190;1;188;0
WireConnection;42;0;179;0
WireConnection;9;0;42;0
WireConnection;9;1;47;0
WireConnection;143;0;3;0
WireConnection;191;0;190;0
WireConnection;142;0;141;0
WireConnection;187;0;9;0
WireConnection;187;1;191;0
WireConnection;46;0;61;0
WireConnection;111;0;142;0
WireConnection;140;0;139;0
WireConnection;145;0;144;0
WireConnection;169;1;140;2
WireConnection;169;2;166;0
WireConnection;63;0;46;4
WireConnection;63;1;46;2
WireConnection;168;0;3;4
WireConnection;168;1;119;0
WireConnection;175;0;187;0
WireConnection;113;0;111;0
WireConnection;113;1;112;0
WireConnection;121;1;113;0
WireConnection;121;2;145;2
WireConnection;64;0;63;0
WireConnection;171;0;46;1
WireConnection;136;0;168;0
WireConnection;136;1;169;0
WireConnection;44;0;1;0
WireConnection;44;1;43;0
WireConnection;152;0;151;0
WireConnection;17;0;136;0
WireConnection;17;1;46;3
WireConnection;17;2;176;0
WireConnection;65;0;117;0
WireConnection;65;1;64;0
WireConnection;59;0;60;0
WireConnection;59;1;171;0
WireConnection;122;0;121;0
WireConnection;122;1;44;0
WireConnection;147;0;146;0
WireConnection;106;0;147;0
WireConnection;106;1;107;0
WireConnection;45;0;17;0
WireConnection;7;0;122;0
WireConnection;7;1;59;0
WireConnection;7;2;178;0
WireConnection;18;0;117;0
WireConnection;18;1;65;0
WireConnection;18;2;177;0
WireConnection;156;1;152;1
WireConnection;156;2;154;0
WireConnection;165;0;45;0
WireConnection;158;0;156;0
WireConnection;160;0;18;0
WireConnection;148;0;106;0
WireConnection;163;0;7;0
WireConnection;0;0;162;0
WireConnection;0;1;161;0
WireConnection;0;3;149;0
WireConnection;0;4;164;0
WireConnection;0;5;159;0
ASEEND*/
//CHKSM=D3D1AF73CF188C452F9E8AE7F16C8DBCCB94E6A9