// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "cfantauzzo/Mossy Forest/Mossy TreeTrunk"
{
	Properties
	{
		_FullColor("Full Color", Color) = (1,1,1,0)
		_Color("Color", Color) = (0,0,0,0)
		_Albedo_Base("Albedo_Base", 2D) = "white" {}
		_Normal_Base("Normal_Base", 2D) = "bump" {}
		_MaskMap("MaskMap", 2D) = "white" {}
		_MossColor("Moss Color", Color) = (0.8470588,1,0.2666667,0)
		_MossDetailmap("Moss Detailmap", 2D) = "white" {}
		[KeywordEnum(UV0,UV1)] _DetailUVMap("Detail UV Map", Float) = 0
		_DetailMap("DetailMap", 2D) = "gray" {}
		_Smoothness("Smoothness", Range( 0 , 1)) = 1
		_DetailSmoothness("Detail Smoothness", Range( 0 , 1)) = 0
		_CoverageRemap2("Coverage Remap 2", Vector) = (0,1,0,1)
		_CoverageDirection("Coverage Direction", Vector) = (1,1,0,0)
		_DetailNormalScale("Detail Normal Scale", Range( 0 , 2)) = 1
		_DetailAlbedo("Detail Albedo", Range( 0 , 2)) = 1
		_CoverageNormalInfluence("Coverage Normal Influence", Range( 0 , 1)) = 1
		_OcclusionStrength("Occlusion Strength", Range( 0 , 1)) = 1
		_AlbedoTiled("Albedo Tiled", 2D) = "white" {}
		_CoverageOcclusionMask("Coverage Occlusion Mask", Range( 0 , 1)) = 0.85
		_MaskMapTiled("MaskMap Tiled", 2D) = "white" {}
		_NormalTiled("Normal Tiled", 2D) = "bump" {}
		[HideInInspector] _texcoord2( "", 2D ) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGINCLUDE
		#include "UnityStandardUtils.cginc"
		#include "UnityShaderVariables.cginc"
		#include "UnityPBSLighting.cginc"
		#include "Lighting.cginc"
		#pragma target 4.6
		#pragma shader_feature_local _DETAILUVMAP_UV0 _DETAILUVMAP_UV1
		#ifdef UNITY_PASS_SHADOWCASTER
			#undef INTERNAL_DATA
			#undef WorldReflectionVector
			#undef WorldNormalVector
			#define INTERNAL_DATA half3 internalSurfaceTtoW0; half3 internalSurfaceTtoW1; half3 internalSurfaceTtoW2;
			#define WorldReflectionVector(data,normal) reflect (data.worldRefl, half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal)))
			#define WorldNormalVector(data,normal) half3(dot(data.internalSurfaceTtoW0,normal), dot(data.internalSurfaceTtoW1,normal), dot(data.internalSurfaceTtoW2,normal))
		#endif
		struct Input
		{
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
			float2 uv2_texcoord2;
			float3 worldPos;
			float3 worldNormal;
			INTERNAL_DATA
		};

		uniform sampler2D _NormalTiled;
		uniform float4 _NormalTiled_ST;
		uniform sampler2D _Normal_Base;
		uniform float4 _Normal_Base_ST;
		uniform sampler2D _DetailMap;
		uniform float4 _DetailMap_ST;
		uniform float _DetailNormalScale;
		uniform sampler2D _MaskMap;
		uniform float4 _MaskMap_ST;
		uniform sampler2D _MossDetailmap;
		uniform float _CoverageNormalInfluence;
		uniform float3 _CoverageDirection;
		uniform float4 _CoverageRemap2;
		uniform sampler2D _MaskMapTiled;
		uniform float4 _MaskMapTiled_ST;
		uniform float _CoverageOcclusionMask;
		uniform float4 _FullColor;
		uniform float _DetailAlbedo;
		uniform sampler2D _AlbedoTiled;
		uniform float4 _AlbedoTiled_ST;
		uniform sampler2D _Albedo_Base;
		uniform float4 _Albedo_Base_ST;
		uniform float4 _Color;
		uniform float4 _MossColor;
		uniform float _Smoothness;
		uniform float _DetailSmoothness;
		uniform float _OcclusionStrength;


		inline float4 TriplanarSampling46( sampler2D topTexMap, float3 worldPos, float3 worldNormal, float falloff, float2 tiling, float3 normalScale, float3 index )
		{
			float3 projNormal = ( pow( abs( worldNormal ), falloff ) );
			projNormal /= ( projNormal.x + projNormal.y + projNormal.z ) + 0.00001;
			float3 nsign = sign( worldNormal );
			half4 xNorm; half4 yNorm; half4 zNorm;
			xNorm = tex2D( topTexMap, tiling * worldPos.zy * float2(  nsign.x, 1.0 ) );
			yNorm = tex2D( topTexMap, tiling * worldPos.xz * float2(  nsign.y, 1.0 ) );
			zNorm = tex2D( topTexMap, tiling * worldPos.xy * float2( -nsign.z, 1.0 ) );
			return xNorm * projNormal.x + yNorm * projNormal.y + zNorm * projNormal.z;
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_NormalTiled = i.uv_texcoord * _NormalTiled_ST.xy + _NormalTiled_ST.zw;
			float2 uv_Normal_Base = i.uv_texcoord * _Normal_Base_ST.xy + _Normal_Base_ST.zw;
			float3 lerpResult192 = lerp( UnpackNormal( tex2D( _NormalTiled, uv_NormalTiled ) ) , UnpackNormal( tex2D( _Normal_Base, uv_Normal_Base ) ) , i.vertexColor.g);
			float2 uv_DetailMap = i.uv_texcoord * _DetailMap_ST.xy + _DetailMap_ST.zw;
			float2 uv2_DetailMap = i.uv2_texcoord2 * _DetailMap_ST.xy + _DetailMap_ST.zw;
			#if defined(_DETAILUVMAP_UV0)
				float2 staticSwitch131 = uv_DetailMap;
			#elif defined(_DETAILUVMAP_UV1)
				float2 staticSwitch131 = uv2_DetailMap;
			#else
				float2 staticSwitch131 = uv_DetailMap;
			#endif
			float4 tex2DNode110 = tex2D( _DetailMap, staticSwitch131 );
			float4 appendResult114 = (float4(tex2DNode110.a , tex2DNode110.g , 1.0 , 1.0));
			float2 uv_MaskMap = i.uv_texcoord * _MaskMap_ST.xy + _MaskMap_ST.zw;
			float4 tex2DNode3 = tex2D( _MaskMap, uv_MaskMap );
			float3 lerpResult117 = lerp( lerpResult192 , BlendNormals( lerpResult192 , UnpackScaleNormal( appendResult114, _DetailNormalScale ) ) , tex2DNode3.b);
			float3 ase_worldPos = i.worldPos;
			float3 ase_worldNormal = WorldNormalVector( i, float3( 0, 0, 1 ) );
			float3 ase_vertex3Pos = mul( unity_WorldToObject, float4( i.worldPos , 1 ) );
			float3 ase_vertexNormal = mul( unity_WorldToObject, float4( ase_worldNormal, 0 ) );
			float4 triplanar46 = TriplanarSampling46( _MossDetailmap, ase_vertex3Pos, ase_vertexNormal, 1.0, float2( 2,2 ), 1.0, 0 );
			float4 appendResult63 = (float4(triplanar46.w , triplanar46.y , 1.0 , 1.0));
			float3 lerpResult219 = lerp( float3( 0,0,1 ) , lerpResult117 , _CoverageNormalInfluence);
			float3 newWorldNormal221 = (WorldNormalVector( i , lerpResult219 ));
			float temp_output_42_0 = saturate( (_CoverageRemap2.z + (( ( newWorldNormal221.x * _CoverageDirection.x ) + ( newWorldNormal221.y * _CoverageDirection.y ) + ( newWorldNormal221.z * _CoverageDirection.z ) ) - _CoverageRemap2.x) * (_CoverageRemap2.w - _CoverageRemap2.z) / (_CoverageRemap2.y - _CoverageRemap2.x)) );
			float2 uv_MaskMapTiled = i.uv_texcoord * _MaskMapTiled_ST.xy + _MaskMapTiled_ST.zw;
			float4 tex2DNode188 = tex2D( _MaskMapTiled, uv_MaskMapTiled );
			float4 MaskMapVar143 = tex2DNode3;
			float lerpResult187 = lerp( tex2DNode188.g , MaskMapVar143.g , i.vertexColor.g);
			float CoverageMaskVar175 = ( temp_output_42_0 * saturate( (0.0 + (lerpResult187 - _CoverageOcclusionMask) * (1.0 - 0.0) / (1.0 - _CoverageOcclusionMask)) ) );
			float3 lerpResult18 = lerp( lerpResult117 , BlendNormals( lerpResult117 , UnpackScaleNormal( appendResult63, 0.9 ) ) , CoverageMaskVar175);
			float3 NormalVarOutput160 = lerpResult18;
			o.Normal = NormalVarOutput160;
			float4 DetailMapVar137 = tex2DNode110;
			float saferPower113 = max( ( DetailMapVar137.r * 2.0 ) , 0.0001 );
			float lerpResult121 = lerp( 0.5 , pow( saferPower113 , _DetailAlbedo ) , MaskMapVar143.b);
			float4 temp_cast_2 = (lerpResult121).xxxx;
			float2 uv2_AlbedoTiled = i.uv2_texcoord2 * _AlbedoTiled_ST.xy + _AlbedoTiled_ST.zw;
			float2 uv_Albedo_Base = i.uv_texcoord * _Albedo_Base_ST.xy + _Albedo_Base_ST.zw;
			float4 lerpResult179 = lerp( tex2D( _AlbedoTiled, uv2_AlbedoTiled ) , ( tex2D( _Albedo_Base, uv_Albedo_Base ) * _Color ) , i.vertexColor.g);
			float4 blendOpSrc122 = temp_cast_2;
			float4 blendOpDest122 = lerpResult179;
			float4 lerpResult7 = lerp( ( _FullColor * ( saturate( (( blendOpDest122 > 0.5 ) ? ( 1.0 - 2.0 * ( 1.0 - blendOpDest122 ) * ( 1.0 - blendOpSrc122 ) ) : ( 2.0 * blendOpDest122 * blendOpSrc122 ) ) )) ) , ( _MossColor * triplanar46.x ) , CoverageMaskVar175);
			float4 AlbedoVarOutput163 = lerpResult7;
			o.Albedo = AlbedoVarOutput163.rgb;
			float lerpResult190 = lerp( tex2DNode188.a , tex2DNode3.a , i.vertexColor.g);
			float lerpResult169 = lerp( 0.5 , DetailMapVar137.b , _DetailSmoothness);
			float blendOpSrc136 = ( lerpResult190 * _Smoothness );
			float blendOpDest136 = lerpResult169;
			float lerpResult17 = lerp( ( saturate( (( blendOpDest136 > 0.5 ) ? ( 1.0 - 2.0 * ( 1.0 - blendOpDest136 ) * ( 1.0 - blendOpSrc136 ) ) : ( 2.0 * blendOpDest136 * blendOpSrc136 ) ) )) , triplanar46.z , CoverageMaskVar175);
			float SmoothnessVarOutput165 = saturate( lerpResult17 );
			o.Smoothness = SmoothnessVarOutput165;
			float lerpResult156 = lerp( 1.0 , lerpResult187 , _OcclusionStrength);
			float OcclusionVarOutput158 = lerpResult156;
			o.Occlusion = OcclusionVarOutput158;
			o.Alpha = 1;
		}

		ENDCG
		CGPROGRAM
		#pragma exclude_renderers xboxseries playstation 
		#pragma surface surf Standard keepalpha fullforwardshadows dithercrossfade 

		ENDCG
		Pass
		{
			Name "ShadowCaster"
			Tags{ "LightMode" = "ShadowCaster" }
			ZWrite On
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 4.6
			#pragma multi_compile_shadowcaster
			#pragma multi_compile UNITY_PASS_SHADOWCASTER
			#pragma skip_variants FOG_LINEAR FOG_EXP FOG_EXP2
			#include "HLSLSupport.cginc"
			#if ( SHADER_API_D3D11 || SHADER_API_GLCORE || SHADER_API_GLES || SHADER_API_GLES3 || SHADER_API_METAL || SHADER_API_VULKAN )
				#define CAN_SKIP_VPOS
			#endif
			#include "UnityCG.cginc"
			#include "Lighting.cginc"
			#include "UnityPBSLighting.cginc"
			struct v2f
			{
				V2F_SHADOW_CASTER;
				float4 customPack1 : TEXCOORD1;
				float4 tSpace0 : TEXCOORD2;
				float4 tSpace1 : TEXCOORD3;
				float4 tSpace2 : TEXCOORD4;
				half4 color : COLOR0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};
			v2f vert( appdata_full v )
			{
				v2f o;
				UNITY_SETUP_INSTANCE_ID( v );
				UNITY_INITIALIZE_OUTPUT( v2f, o );
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );
				UNITY_TRANSFER_INSTANCE_ID( v, o );
				Input customInputData;
				float3 worldPos = mul( unity_ObjectToWorld, v.vertex ).xyz;
				half3 worldNormal = UnityObjectToWorldNormal( v.normal );
				half3 worldTangent = UnityObjectToWorldDir( v.tangent.xyz );
				half tangentSign = v.tangent.w * unity_WorldTransformParams.w;
				half3 worldBinormal = cross( worldNormal, worldTangent ) * tangentSign;
				o.tSpace0 = float4( worldTangent.x, worldBinormal.x, worldNormal.x, worldPos.x );
				o.tSpace1 = float4( worldTangent.y, worldBinormal.y, worldNormal.y, worldPos.y );
				o.tSpace2 = float4( worldTangent.z, worldBinormal.z, worldNormal.z, worldPos.z );
				o.customPack1.xy = customInputData.uv_texcoord;
				o.customPack1.xy = v.texcoord;
				o.customPack1.zw = customInputData.uv2_texcoord2;
				o.customPack1.zw = v.texcoord1;
				TRANSFER_SHADOW_CASTER_NORMALOFFSET( o )
				o.color = v.color;
				return o;
			}
			half4 frag( v2f IN
			#if !defined( CAN_SKIP_VPOS )
			, UNITY_VPOS_TYPE vpos : VPOS
			#endif
			) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				Input surfIN;
				UNITY_INITIALIZE_OUTPUT( Input, surfIN );
				surfIN.uv_texcoord = IN.customPack1.xy;
				surfIN.uv2_texcoord2 = IN.customPack1.zw;
				float3 worldPos = float3( IN.tSpace0.w, IN.tSpace1.w, IN.tSpace2.w );
				half3 worldViewDir = normalize( UnityWorldSpaceViewDir( worldPos ) );
				surfIN.worldPos = worldPos;
				surfIN.worldNormal = float3( IN.tSpace0.z, IN.tSpace1.z, IN.tSpace2.z );
				surfIN.internalSurfaceTtoW0 = IN.tSpace0.xyz;
				surfIN.internalSurfaceTtoW1 = IN.tSpace1.xyz;
				surfIN.internalSurfaceTtoW2 = IN.tSpace2.xyz;
				surfIN.vertexColor = IN.color;
				SurfaceOutputStandard o;
				UNITY_INITIALIZE_OUTPUT( SurfaceOutputStandard, o )
				surf( surfIN, o );
				#if defined( CAN_SKIP_VPOS )
				float2 vpos = IN.pos;
				#endif
				SHADOW_CASTER_FRAGMENT( IN )
			}
			ENDCG
		}
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18909
22.66667;186.6667;1920;529;2466.372;540.4161;1.826105;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;135;-3281.479,1043.798;Inherit;False;1;110;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;132;-3281.879,908.1995;Inherit;False;0;110;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;131;-3004.62,964.2653;Inherit;False;Property;_DetailUVMap;Detail UV Map;8;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;2;UV0;UV1;Create;True;True;9;1;FLOAT2;0,0;False;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT2;0,0;False;6;FLOAT2;0,0;False;7;FLOAT2;0,0;False;8;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;110;-2783.18,939.5269;Inherit;True;Property;_DetailMap;DetailMap;9;0;Create;True;0;0;0;False;0;False;-1;None;de13c6c2fca5fe0408df18fb9b387956;True;1;False;gray;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;191;-1979.911,304.0459;Inherit;True;Property;_NormalTiled;Normal Tiled;27;0;Create;True;0;0;0;False;0;False;-1;b472cf4f547608b489ed214d88f890c2;b472cf4f547608b489ed214d88f890c2;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;114;-2093.169,819.7097;Inherit;True;COLOR;4;0;FLOAT;1;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.VertexColorNode;193;-1872.131,601.6194;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;115;-2155.388,1071.61;Inherit;False;Property;_DetailNormalScale;Detail Normal Scale;14;0;Create;True;0;0;0;False;0;False;1;1;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-3553.439,503.562;Inherit;True;Property;_Normal_Base;Normal_Base;3;0;Create;True;0;0;0;False;0;False;-1;None;638b5d1a0e8941d4f8279d89170fdd4e;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;192;-1504.539,434.8567;Inherit;True;3;0;FLOAT3;0,0,1;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.UnpackScaleNormalNode;116;-1816.162,782.5427;Inherit;True;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.BlendNormalsNode;129;-893.9035,511.3391;Inherit;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;3;-2111.882,1270.252;Inherit;True;Property;_MaskMap;MaskMap;4;0;Create;True;0;0;0;False;0;False;-1;None;103e554cc082f184b87c3df690f34b9f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;218;-3503.993,-243.4356;Inherit;False;Property;_CoverageNormalInfluence;Coverage Normal Influence;17;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;117;-551.5366,435.2665;Inherit;True;3;0;FLOAT3;0,0,1;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;219;-3158.315,-304.0915;Inherit;True;3;0;FLOAT3;0,0,1;False;1;FLOAT3;0,0,1;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;143;-1587.961,1267.673;Inherit;False;MaskMapVar;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;151;-969.4927,2223.706;Inherit;False;143;MaskMapVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.Vector3Node;220;-2851.768,-163.1597;Inherit;False;Property;_CoverageDirection;Coverage Direction;13;0;Create;True;0;0;0;False;0;False;1,1,0;1,3,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;221;-2883.768,-339.1597;Inherit;False;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;173;-2648.172,-387.9203;Inherit;False;1808.944;670.7647;;16;214;9;85;91;47;175;216;42;215;227;225;226;213;222;223;224;Coverage Mask;1,1,1,1;0;0
Node;AmplifyShaderEditor.VertexColorNode;189;-1231.16,1942.393;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;222;-2509.961,-161.2246;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;188;-1384.399,1627.779;Inherit;True;Property;_MaskMapTiled;MaskMap Tiled;23;0;Create;True;0;0;0;False;0;False;-1;f43aa66216d1dd644afb4923916ebdbf;f43aa66216d1dd644afb4923916ebdbf;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BreakToComponentsNode;152;-714.2975,2193.313;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;224;-2509.961,-337.2247;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;223;-2509.961,-257.2247;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;137;-2457.151,829.781;Inherit;False;DetailMapVar;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;187;-445.4682,2092.086;Inherit;True;3;0;FLOAT;1;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;213;-1688.843,86.73149;Inherit;False;Property;_CoverageOcclusionMask;Coverage Occlusion Mask;22;0;Create;True;0;0;0;False;0;False;0.85;0.5;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;226;-2293.751,-210.7157;Inherit;False;Property;_CoverageRemap2;Coverage Remap 2;12;0;Create;True;0;0;0;False;0;False;0,1,0,1;0,1,0,1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;225;-2281.478,-334.676;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;141;-1809.697,-1842.559;Inherit;False;137;DetailMapVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;142;-1589.007,-1838.181;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.TFHCRemapNode;227;-1998.988,-283.7386;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;214;-1376.492,55.76832;Inherit;True;5;0;FLOAT;0;False;1;FLOAT;-10;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;111;-1419.481,-1839.502;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;43;-1049.995,-1091.445;Float;False;Property;_Color;Color;1;0;Create;True;0;0;0;False;0;False;0,0,0,0;1,0.9427992,0.9117647,1;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;42;-1606.463,-74.90102;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;144;-1142.674,-1535.742;Inherit;False;143;MaskMapVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;139;-445.541,1556.169;Inherit;False;137;DetailMapVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.TexturePropertyNode;61;-1601.248,-617.3195;Inherit;True;Property;_MossDetailmap;Moss Detailmap;7;0;Create;True;0;0;0;False;0;False;843c111985eec614a981078bb54447fd;843c111985eec614a981078bb54447fd;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.RangedFloatNode;112;-1497.524,-1572.867;Inherit;False;Property;_DetailAlbedo;Detail Albedo;15;0;Create;True;0;0;0;False;0;False;1;1.25;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;215;-1100.893,65.46575;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-1092.579,-1316.283;Inherit;True;Property;_Albedo_Base;Albedo_Base;2;0;Create;True;0;0;0;False;0;False;-1;None;189a45a4a38d93d418feb6ef120b485d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;190;-856.4357,1267.031;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;166;-357.9163,1700.256;Inherit;False;Property;_DetailSmoothness;Detail Smoothness;11;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TriplanarNode;46;-1359.705,-612.2468;Inherit;True;Spherical;Object;False;Albedo_Cover;_Albedo_Cover;white;4;None;Mid Texture 3;_MidTexture3;white;11;None;Bot Texture 3;_BotTexture3;white;12;None;Albedo_Cover;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;2,2;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;-656.9186,-1289.291;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BreakToComponentsNode;145;-948.9481,-1530.252;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;119;-698.3378,1435.189;Inherit;False;Property;_Smoothness;Smoothness;10;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;113;-1125.056,-1796.579;Inherit;True;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;180;-923.0473,-862.3165;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;216;-1266.556,-125.2254;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;181;-679.7461,-1173.409;Inherit;True;Property;_AlbedoTiled;Albedo Tiled;21;0;Create;True;0;0;0;False;0;False;-1;405d914c6adb872419068a58f5e6c919;405d914c6adb872419068a58f5e6c919;True;1;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BreakToComponentsNode;140;-213.9091,1540.696;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.LerpOp;169;-65.81293,1573.197;Inherit;True;3;0;FLOAT;0.5;False;1;FLOAT;0.5;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;121;-732.887,-1768.375;Inherit;True;3;0;FLOAT;0.5;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;63;-404.5817,228.7322;Inherit;False;COLOR;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;179;-218.7352,-1413.773;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;1,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;175;-1090.985,-152.3916;Inherit;False;CoverageMaskVar;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;168;-282.0421,1292.408;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;136;218.7203,1384.085;Inherit;True;Overlay;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendOpsNode;122;123.1493,-1431.398;Inherit;True;Overlay;True;3;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;211;247.0784,-1634.895;Inherit;False;Property;_FullColor;Full Color;0;0;Create;True;0;0;0;False;0;False;1,1,1,0;1,0.9311663,0.894,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.UnpackScaleNormalNode;64;-229.9521,222.3338;Inherit;False;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0.9;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WireNode;171;-104.5749,-706.3901;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;60;-57.48898,-1173.462;Inherit;False;Property;_MossColor;Moss Color;6;0;Create;True;0;0;0;False;0;False;0.8470588,1,0.2666667,0;0.8470588,1,0.2666667,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;176;-99.50653,682.0158;Inherit;False;175;CoverageMaskVar;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;212;544.0212,-1329.793;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendNormalsNode;65;110.1735,300.7207;Inherit;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;177;235.2771,548.447;Inherit;False;175;CoverageMaskVar;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;17;267.8219,630.7441;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;154;-511.3921,2448.608;Inherit;False;Property;_OcclusionStrength;Occlusion Strength;20;0;Create;True;0;0;0;False;0;False;1;0.75;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;178;250.1835,-830.1445;Inherit;False;175;CoverageMaskVar;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;200.2183,-1106.193;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;7;722.4281,-1198.822;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;156;-101.0044,2159.611;Inherit;True;3;0;FLOAT;1;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;18;484.9792,332.099;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;45;481.1111,655.5397;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;160;669.6959,327.3744;Inherit;False;NormalVarOutput;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;165;677.5251,644.5402;Inherit;False;SmoothnessVarOutput;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;163;940.1968,-1177.045;Inherit;False;AlbedoVarOutput;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;158;183.1541,2067.359;Inherit;False;OcclusionVarOutput;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;210;991.4858,867.7679;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;85;-2598.172,53.13388;Inherit;False;Property;_CoverageMaskTiling;Coverage Mask Tiling;16;0;Create;True;0;0;0;False;0;False;1;0.05;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;149;1981.699,148.6953;Inherit;False;148;MetallicVarOutput;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;205;1691.321,2085.291;Float;False;Constant;_Color0;Color 0;18;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;204;1600.297,2297.18;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;162;1982.987,-20.30891;Inherit;False;163;AlbedoVarOutput;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;207;1805.745,2348.322;Inherit;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalVertexDataNode;196;748.8481,1195.033;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;206;1567.126,1178.745;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;-1451.307,-51.78494;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;148;-47.42117,2836.409;Inherit;False;MetallicVarOutput;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;147;-442.6452,2767.222;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;107;-536.455,2959.075;Inherit;False;Property;_MetallicStrength;Metallic Strength;19;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;146;-661.2222,2777.327;Inherit;False;143;MaskMapVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;159;1978.373,294.729;Inherit;False;158;OcclusionVarOutput;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;209;596.9909,758.0249;Inherit;True;Property;_DisplacementBase;Displacement Base;25;0;Create;True;0;0;0;False;0;False;-1;None;665f229f490c23649a3fe9a4a9007050;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;199;749.02,1521.58;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;195;601.9765,960.3463;Inherit;True;Property;_DisplacementTiled;Displacement Tiled;18;0;Create;True;0;0;0;False;0;False;-1;None;e1155ce415adf77419cd3b0dd18dbacc;True;1;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TriplanarNode;47;-2079.348,-6.772395;Inherit;True;Spherical;World;False;Coverage Mask;_CoverageMask;white;5;None;Mid Texture 1;_MidTexture1;white;11;None;Bot Texture 1;_BotTexture1;white;12;None;Coverage Mask;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;197;1235.644,1098.044;Float;False;Property;_Displacement;Displacement;26;0;Create;True;0;0;0;False;0;False;0;0.12;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;161;1986.935,66.33771;Inherit;False;160;NormalVarOutput;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;208;2016.707,1941.184;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;106;-192.1419,2835.253;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;200;1034.134,1313.371;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;194;-1927.626,-520.1411;Inherit;False;Property;_MossTiling;Moss Tiling;28;0;Create;True;0;0;0;False;0;False;1;0;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;164;1972.813,222.226;Inherit;False;165;SmoothnessVarOutput;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;198;1251.031,956.6404;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;201;971.8649,1463.594;Float;False;Property;_Midpoint;Midpoint;24;0;Create;True;0;0;0;False;0;False;0;-0.031;-1;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;202;1608.906,950.3466;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;203;1379.09,1284.873;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;91;-2282.288,40.35715;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.LerpOp;230;1894.345,843.0233;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;2321.348,65.17906;Float;False;True;-1;6;ASEMaterialInspector;0;0;Standard;cfantauzzo/Mossy Forest/Mossy TreeTrunk;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Opaque;0.5;True;True;0;False;Opaque;;Geometry;All;14;d3d9;d3d11_9x;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;ps4;psp2;n3ds;wiiu;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;0;30;10;11;True;0.55;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;1;False;-1;1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
Node;AmplifyShaderEditor.CommentaryNode;150;-704.938,2717.222;Inherit;False;902.8506;363.1353;;0;Metallic;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;157;-994.144,1977.584;Inherit;False;1448.501;626.5454;;0;Occlusion;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;170;-1055.307,1163.855;Inherit;False;1589.685;701.8792;;0;Smoothness;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;172;-1859.697,-1892.559;Inherit;False;3258.29;1215.963;;0;Albedo;1,1,1,1;0;0
WireConnection;131;1;132;0
WireConnection;131;0;135;0
WireConnection;110;1;131;0
WireConnection;114;0;110;4
WireConnection;114;1;110;2
WireConnection;192;0;191;0
WireConnection;192;1;2;0
WireConnection;192;2;193;2
WireConnection;116;0;114;0
WireConnection;116;1;115;0
WireConnection;129;0;192;0
WireConnection;129;1;116;0
WireConnection;117;0;192;0
WireConnection;117;1;129;0
WireConnection;117;2;3;3
WireConnection;219;1;117;0
WireConnection;219;2;218;0
WireConnection;143;0;3;0
WireConnection;221;0;219;0
WireConnection;222;0;221;3
WireConnection;222;1;220;3
WireConnection;152;0;151;0
WireConnection;224;0;221;1
WireConnection;224;1;220;1
WireConnection;223;0;221;2
WireConnection;223;1;220;2
WireConnection;137;0;110;0
WireConnection;187;0;188;2
WireConnection;187;1;152;1
WireConnection;187;2;189;2
WireConnection;225;0;224;0
WireConnection;225;1;223;0
WireConnection;225;2;222;0
WireConnection;142;0;141;0
WireConnection;227;0;225;0
WireConnection;227;1;226;1
WireConnection;227;2;226;2
WireConnection;227;3;226;3
WireConnection;227;4;226;4
WireConnection;214;0;187;0
WireConnection;214;1;213;0
WireConnection;111;0;142;0
WireConnection;42;0;227;0
WireConnection;215;0;214;0
WireConnection;190;0;188;4
WireConnection;190;1;3;4
WireConnection;190;2;189;2
WireConnection;46;0;61;0
WireConnection;44;0;1;0
WireConnection;44;1;43;0
WireConnection;145;0;144;0
WireConnection;113;0;111;0
WireConnection;113;1;112;0
WireConnection;216;0;42;0
WireConnection;216;1;215;0
WireConnection;140;0;139;0
WireConnection;169;1;140;2
WireConnection;169;2;166;0
WireConnection;121;1;113;0
WireConnection;121;2;145;2
WireConnection;63;0;46;4
WireConnection;63;1;46;2
WireConnection;179;0;181;0
WireConnection;179;1;44;0
WireConnection;179;2;180;2
WireConnection;175;0;216;0
WireConnection;168;0;190;0
WireConnection;168;1;119;0
WireConnection;136;0;168;0
WireConnection;136;1;169;0
WireConnection;122;0;121;0
WireConnection;122;1;179;0
WireConnection;64;0;63;0
WireConnection;171;0;46;1
WireConnection;212;0;211;0
WireConnection;212;1;122;0
WireConnection;65;0;117;0
WireConnection;65;1;64;0
WireConnection;17;0;136;0
WireConnection;17;1;46;3
WireConnection;17;2;176;0
WireConnection;59;0;60;0
WireConnection;59;1;171;0
WireConnection;7;0;212;0
WireConnection;7;1;59;0
WireConnection;7;2;178;0
WireConnection;156;1;187;0
WireConnection;156;2;154;0
WireConnection;18;0;117;0
WireConnection;18;1;65;0
WireConnection;18;2;177;0
WireConnection;45;0;17;0
WireConnection;160;0;18;0
WireConnection;165;0;45;0
WireConnection;163;0;7;0
WireConnection;158;0;156;0
WireConnection;210;0;195;0
WireConnection;210;1;209;0
WireConnection;210;2;199;2
WireConnection;204;0;199;0
WireConnection;204;1;199;2
WireConnection;207;0;204;0
WireConnection;207;1;199;3
WireConnection;206;1;203;0
WireConnection;9;0;42;0
WireConnection;9;1;47;0
WireConnection;148;0;106;0
WireConnection;147;0;146;0
WireConnection;47;3;91;0
WireConnection;208;1;205;0
WireConnection;208;2;207;0
WireConnection;106;0;147;0
WireConnection;106;1;107;0
WireConnection;198;0;210;0
WireConnection;198;1;196;0
WireConnection;202;0;198;0
WireConnection;202;1;197;0
WireConnection;203;0;200;0
WireConnection;203;1;201;0
WireConnection;91;0;85;0
WireConnection;91;1;85;0
WireConnection;230;1;202;0
WireConnection;230;2;209;4
WireConnection;0;0;162;0
WireConnection;0;1;161;0
WireConnection;0;4;164;0
WireConnection;0;5;159;0
ASEEND*/
//CHKSM=8B40D45C0812ECC07C6A9FD2DEA2D8A99B876E31