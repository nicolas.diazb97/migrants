// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "cfantauzzo/Mossy Forest/Leaves"
{
	Properties
	{
		_Color("Color", Color) = (0,0,0,0)
		_ColorVariation("Color Variation", Color) = (1,0.9268935,0.754717,0)
		_ColorVariationScale("Color Variation Scale", Range( 0 , 10)) = 1
		_ColorVariationRemap("Color Variation Remap", Vector) = (0,1,0,0)
		_OpacityMip("Opacity Mip", Int) = 0
		_Normal("Normal", 2D) = "bump" {}
		_Cutoff( "Mask Clip Value", Float ) = 0.5
		_NormalScale("Normal Scale", Range( 0 , 1)) = 0
		_Metallic("Metallic", 2D) = "white" {}
		_SmoothnessValue("Smoothness Value", Range( 0 , 1)) = 0
		_Occlusion("Occlusion", 2D) = "white" {}
		_OcclusionValue("Occlusion Value", Range( 0 , 1)) = 0
		_TranslucencyMap("Translucency Map", 2D) = "white" {}
		_TranslucencyColor("Translucency Color", Color) = (0,0,0,0)
		[Header(Translucency)]
		_Translucency("Strength", Range( 0 , 50)) = 1
		_TransNormalDistortion("Normal Distortion", Range( 0 , 1)) = 0.1
		_TransScattering("Scaterring Falloff", Range( 1 , 50)) = 2
		_TransDirect("Direct", Range( 0 , 1)) = 1
		_TransAmbient("Ambient", Range( 0 , 1)) = 0.2
		_TransShadow("Shadow", Range( 0 , 1)) = 0.9
		_WindMap("Wind Map", 2D) = "white" {}
		_Albedo("Albedo", 2D) = "white" {}
		_WindValue("Wind Value", Range( 0 , 1)) = 0
		_WindSpeed("Wind Speed", Range( 0 , 5)) = 0
		_WorldNormalWindInfluence("World Normal Wind Influence", Range( 0 , 3)) = 0
		_VertexNormalWindInfluence("Vertex Normal Wind Influence", Range( 0 , 1)) = 0
		_UVDistortionTiling("UV Distortion Tiling", Range( 0 , 10)) = 1
		_UVDistortionSpeed("UV Distortion Speed", Range( 0 , 5)) = 0
		_UVDIstortionStrength("UV DIstortion Strength", Range( 0 , 2)) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#include "UnityStandardUtils.cginc"
		#include "UnityPBSLighting.cginc"
		#pragma target 3.0
		#pragma exclude_renderers xboxseries playstation 
		#pragma surface surf StandardCustom keepalpha addshadow fullforwardshadows exclude_path:deferred dithercrossfade vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
			float4 vertexColor : COLOR;
		};

		struct SurfaceOutputStandardCustom
		{
			half3 Albedo;
			half3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			half Alpha;
			half3 Translucency;
		};

		uniform sampler2D _WindMap;
		uniform float _WindSpeed;
		uniform float _WindValue;
		uniform float _WorldNormalWindInfluence;
		uniform float _VertexNormalWindInfluence;
		uniform sampler2D _Normal;
		uniform float _UVDistortionSpeed;
		uniform float _UVDistortionTiling;
		uniform float _UVDIstortionStrength;
		uniform float _NormalScale;
		uniform sampler2D _Albedo;
		uniform float4 _Color;
		uniform float4 _ColorVariation;
		uniform float _ColorVariationScale;
		uniform float2 _ColorVariationRemap;
		uniform sampler2D _Metallic;
		uniform float _SmoothnessValue;
		uniform sampler2D _Occlusion;
		uniform float _OcclusionValue;
		uniform half _Translucency;
		uniform half _TransNormalDistortion;
		uniform half _TransScattering;
		uniform half _TransDirect;
		uniform half _TransAmbient;
		uniform half _TransShadow;
		uniform sampler2D _TranslucencyMap;
		uniform float4 _TranslucencyColor;
		uniform int _OpacityMip;
		uniform float _Cutoff = 0.5;


		inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }

		inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }

		inline float valueNoise (float2 uv)
		{
			float2 i = floor(uv);
			float2 f = frac( uv );
			f = f* f * (3.0 - 2.0 * f);
			uv = abs( frac(uv) - 0.5);
			float2 c0 = i + float2( 0.0, 0.0 );
			float2 c1 = i + float2( 1.0, 0.0 );
			float2 c2 = i + float2( 0.0, 1.0 );
			float2 c3 = i + float2( 1.0, 1.0 );
			float r0 = noise_randomValue( c0 );
			float r1 = noise_randomValue( c1 );
			float r2 = noise_randomValue( c2 );
			float r3 = noise_randomValue( c3 );
			float bottomOfGrid = noise_interpolate( r0, r1, f.x );
			float topOfGrid = noise_interpolate( r2, r3, f.x );
			float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
			return t;
		}


		float SimpleNoise(float2 UV)
		{
			float t = 0.0;
			float freq = pow( 2.0, float( 0 ) );
			float amp = pow( 0.5, float( 3 - 0 ) );
			t += valueNoise( UV/freq )*amp;
			freq = pow(2.0, float(1));
			amp = pow(0.5, float(3-1));
			t += valueNoise( UV/freq )*amp;
			freq = pow(2.0, float(2));
			amp = pow(0.5, float(3-2));
			t += valueNoise( UV/freq )*amp;
			return t;
		}


		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float2 panner16 = ( ( _Time * _WindSpeed ).x * float2( 1,1 ) + v.texcoord1.xy);
			float3 ase_worldNormal = UnityObjectToWorldNormal( v.normal );
			float3 ase_vertexNormal = v.normal.xyz;
			float4 temp_cast_2 = (v.color.r).xxxx;
			v.vertex.xyz += ( ( ( tex2Dlod( _WindMap, float4( panner16, 0, 0.0) ) * _WindValue ) * float4( ( ( ase_worldNormal * _WorldNormalWindInfluence ) + ( ase_vertexNormal * _VertexNormalWindInfluence ) ) , 0.0 ) ) * ( temp_cast_2 - v.color ) ).rgb;
			v.vertex.w = 1;
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float3 ase_worldViewDir = normalize( UnityWorldSpaceViewDir( ase_worldPos ) );
			float dotResult79 = dot( ase_worldNormal , ase_worldViewDir );
			float3 lerpResult83 = lerp( ase_vertexNormal , -ase_vertexNormal , (1.0 + (sign( dotResult79 ) - -1.0) * (0.0 - 1.0) / (1.0 - -1.0)));
			v.normal = lerpResult83;
		}

		inline half4 LightingStandardCustom(SurfaceOutputStandardCustom s, half3 viewDir, UnityGI gi )
		{
			#if !defined(DIRECTIONAL)
			float3 lightAtten = gi.light.color;
			#else
			float3 lightAtten = lerp( _LightColor0.rgb, gi.light.color, _TransShadow );
			#endif
			half3 lightDir = gi.light.dir + s.Normal * _TransNormalDistortion;
			half transVdotL = pow( saturate( dot( viewDir, -lightDir ) ), _TransScattering );
			half3 translucency = lightAtten * (transVdotL * _TransDirect + gi.indirect.diffuse * _TransAmbient) * s.Translucency;
			half4 c = half4( s.Albedo * translucency * _Translucency, 0 );

			SurfaceOutputStandard r;
			r.Albedo = s.Albedo;
			r.Normal = s.Normal;
			r.Emission = s.Emission;
			r.Metallic = s.Metallic;
			r.Smoothness = s.Smoothness;
			r.Occlusion = s.Occlusion;
			r.Alpha = s.Alpha;
			return LightingStandard (r, viewDir, gi) + c;
		}

		inline void LightingStandardCustom_GI(SurfaceOutputStandardCustom s, UnityGIInput data, inout UnityGI gi )
		{
			#if defined(UNITY_PASS_DEFERRED) && UNITY_ENABLE_REFLECTION_BUFFERS
				gi = UnityGlobalIllumination(data, s.Occlusion, s.Normal);
			#else
				UNITY_GLOSSY_ENV_FROM_SURFACE( g, s, data );
				gi = UnityGlobalIllumination( data, s.Occlusion, s.Normal, g );
			#endif
		}

		void surf( Input i , inout SurfaceOutputStandardCustom o )
		{
			float2 temp_cast_3 = (_UVDistortionTiling).xx;
			float2 uv_TexCoord108 = i.uv_texcoord * temp_cast_3;
			float2 panner117 = ( ( _Time * _UVDistortionSpeed ).x * float2( 1,1 ) + uv_TexCoord108);
			float4 temp_cast_4 = (i.vertexColor.r).xxxx;
			float4 lerpResult118 = lerp( float4( i.uv_texcoord, 0.0 , 0.0 ) , ( float4( i.uv_texcoord, 0.0 , 0.0 ) + ( tex2D( _WindMap, panner117 ) * _UVDIstortionStrength ) ) , ( temp_cast_4 - i.vertexColor ));
			float4 UVDistortVar109 = lerpResult118;
			o.Normal = UnpackScaleNormal( tex2D( _Normal, UVDistortVar109.rg ), _NormalScale );
			float3 ase_worldPos = i.worldPos;
			float simpleNoise84 = SimpleNoise( ase_worldPos.xy*_ColorVariationScale );
			float4 lerpResult87 = lerp( _Color , _ColorVariation , (_ColorVariationRemap.x + (simpleNoise84 - 0.0) * (_ColorVariationRemap.y - _ColorVariationRemap.x) / (1.0 - 0.0)));
			o.Albedo = ( tex2Dlod( _Albedo, float4( UVDistortVar109.rg, 0, 0.0) ) * lerpResult87 ).rgb;
			float4 temp_cast_11 = (_OcclusionValue).xxxx;
			float4 temp_output_73_0 = saturate( pow( ( tex2D( _Occlusion, UVDistortVar109.rg ) * i.vertexColor ) , temp_cast_11 ) );
			o.Smoothness = ( ( tex2D( _Metallic, UVDistortVar109.rg ).a * _SmoothnessValue ) * temp_output_73_0 ).r;
			o.Occlusion = temp_output_73_0.r;
			o.Translucency = ( tex2D( _TranslucencyMap, UVDistortVar109.rg ) * _TranslucencyColor ).rgb;
			o.Alpha = 1;
			clip( tex2Dlod( _Albedo, float4( UVDistortVar109.rg, 0, (float)_OpacityMip) ).a - _Cutoff );
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18909
22.66667;186.6667;1920;529;3133.683;1555.389;1.844685;True;True
Node;AmplifyShaderEditor.TimeNode;114;-2382.586,-980.8954;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;113;-2475.024,-778.7386;Float;False;Property;_UVDistortionSpeed;UV Distortion Speed;28;0;Create;True;0;0;0;False;0;False;0;5;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;104;-2568.336,-1118.544;Inherit;False;Property;_UVDistortionTiling;UV Distortion Tiling;27;0;Create;True;0;0;0;False;0;False;1;1.66;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;115;-2134.594,-910.2873;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;108;-2252.376,-1132.494;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TexturePropertyNode;101;-1630.414,240.7247;Inherit;True;Property;_WindMap;Wind Map;21;0;Create;True;0;0;0;False;0;False;None;538b4803186989045ae8015ddb13148f;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.PannerNode;117;-1894.921,-975.8981;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;102;-1364.34,-1313.314;Inherit;True;Property;_TextureSample1;Texture Sample 1;21;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;105;-1349.974,-1089.423;Inherit;False;Property;_UVDIstortionStrength;UV DIstortion Strength;29;0;Create;True;0;0;0;False;0;False;1;0.119;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;106;-1042.835,-1335.933;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.VertexColorNode;119;-1024.746,-1129.546;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;103;-1232.714,-1460.185;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;120;-743.5842,-1124.408;Inherit;False;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;97;-865.6954,-1491.148;Inherit;True;2;2;0;FLOAT2;0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;118;-598.1815,-1420.975;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;99;-1875.701,659.941;Inherit;False;1888.481;978.4598;;19;26;24;25;53;63;7;16;57;58;5;62;15;8;31;64;38;6;27;32;Vertex Displacement;1,1,1,1;0;0
Node;AmplifyShaderEditor.TimeNode;24;-1769.927,877.7662;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;26;-1825.701,1079.923;Float;False;Property;_WindSpeed;Wind Speed;24;0;Create;True;0;0;0;False;0;False;0;3.58;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;109;-360.9876,-1489.681;Inherit;False;UVDistortVar;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;98;349.3624,892.9614;Inherit;False;1012.682;567.3719;;8;77;78;79;80;74;81;76;83;Invert Normals;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;110;-2341.663,-354.7446;Inherit;False;109;UVDistortVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.TexCoordVertexDataNode;27;-1628.296,741.9376;Inherit;False;1;2;0;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-1485.271,948.3743;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;63;-1527.072,1392.164;Float;False;Property;_VertexNormalWindInfluence;Vertex Normal Wind Influence;26;0;Create;True;0;0;0;False;0;False;0;0.3;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;88;-319.7516,-984.2539;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;90;-414.4892,-814.4608;Inherit;False;Property;_ColorVariationScale;Color Variation Scale;2;0;Create;True;0;0;0;False;0;False;1;6.5;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;57;-1043.832,1523.4;Float;False;Property;_WorldNormalWindInfluence;World Normal Wind Influence;25;0;Create;True;0;0;0;False;0;False;0;0.4;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;12;-1973.718,-218.6203;Inherit;True;Property;_Occlusion;Occlusion;10;0;Create;True;0;0;0;False;0;False;-1;None;882ec94634315794ebcd8cba0705fc20;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NormalVertexDataNode;7;-1522.878,1143.858;Inherit;True;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldNormalVector;77;399.3624,1095.913;Inherit;False;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.PannerNode;16;-1245.598,882.7634;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;1,1;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.VertexColorNode;39;-1857.388,-9.650584;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldNormalVector;53;-1063.153,1350.101;Inherit;False;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;78;419.145,1276.333;Float;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;62;-1117.284,1190.982;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;15;-939.891,1054.526;Float;False;Property;_WindValue;Wind Value;23;0;Create;True;0;0;0;False;0;False;0;0.406;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;79;637.5275,1171.776;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;84;-94.782,-901.3827;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-1609.55,-79.62846;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;5;-984.6358,808.6391;Inherit;True;Property;_WindMapSampleDisplacement;Wind Map Sample Displacement;14;0;Create;True;0;0;0;False;0;False;-1;None;538b4803186989045ae8015ddb13148f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;93;-88.81232,-672.8029;Inherit;False;Property;_ColorVariationRemap;Color Variation Remap;3;0;Create;True;0;0;0;False;0;False;0,1;0,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;58;-736.64,1278.238;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-1657.06,52.2222;Float;False;Property;_OcclusionValue;Occlusion Value;11;0;Create;True;0;0;0;False;0;False;0;0.3;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;47;-1356.553,-60.96825;Inherit;False;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalVertexDataNode;74;734.4422,942.9614;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;91;236.5413,-930.416;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;95;-507.143,-1912.143;Inherit;True;Property;_Albedo;Albedo;22;0;Create;True;0;0;0;False;0;False;None;48f723e162dac5c45b654e83bf9dd3d5;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.CommentaryNode;100;-1008.608,122.2446;Inherit;False;868.7692;458.6883;;3;28;71;72;Translucency;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;14;-1690.521,-419.1412;Inherit;True;Property;_Metallic;Metallic;8;0;Create;True;0;0;0;False;0;False;-1;None;d87b8e8ed91866d40a0a0bda1823948a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;18;-1496.04,-171.7754;Float;False;Property;_SmoothnessValue;Smoothness Value;9;0;Create;True;0;0;0;False;0;False;0;0.8;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;66;52.08481,-1310.181;Float;False;Property;_Color;Color;0;0;Create;True;0;0;0;False;0;False;0,0,0,0;0.6118677,0.711,0.5709329,1;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SignOpNode;80;775.0844,1171.67;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;64;-568.2941,1088.911;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;86;42.54385,-1118.181;Inherit;False;Property;_ColorVariation;Color Variation;1;0;Create;True;0;0;0;False;0;False;1,0.9268935,0.754717,0;1,0.6779732,0.5377356,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-577.9539,851.3642;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;111;-1225.054,212.8932;Inherit;False;109;UVDistortVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.VertexColorNode;31;-543.2217,1221.978;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;69;34.53196,-298.6632;Float;False;Property;_NormalScale;Normal Scale;7;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;112;87.49024,-392.1077;Inherit;False;109;UVDistortVar;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;38;-357.9598,1156.416;Inherit;False;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;87;311.7816,-1245.285;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;73;-1176.441,-82.51494;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;28;-953.1691,179.8028;Inherit;True;Property;_TranslucencyMap;Translucency Map;12;0;Create;True;0;0;0;False;0;False;-1;None;48f723e162dac5c45b654e83bf9dd3d5;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-1145.992,-210.2549;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;71;-737.9465,391.1999;Float;False;Property;_TranslucencyColor;Translucency Color;13;0;Create;True;0;0;0;False;0;False;0,0,0,0;1,0.8170386,0.6764706,1;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NegateNode;76;954.4025,1086.334;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TFHCRemapNode;81;912.3115,1170.304;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;-1;False;2;FLOAT;1;False;3;FLOAT;1;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;6;-376.0897,883.843;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.IntNode;94;-369.5759,-1685.579;Inherit;False;Property;_OpacityMip;Opacity Mip;4;0;Create;True;0;0;0;False;0;False;0;2;False;0;1;INT;0
Node;AmplifyShaderEditor.SamplerNode;2;-31.5487,-1519.519;Inherit;True;Property;_Albedoasdf;Albedoasdf;5;0;Create;True;0;0;0;False;0;False;-1;None;48f723e162dac5c45b654e83bf9dd3d5;True;0;False;white;Auto;False;Object;-1;MipLevel;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;72;-301.8387,375.3867;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;65;491.8114,-1309.867;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;-163.0145,891.0862;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TriplanarNode;122;-1280.125,-898.379;Inherit;True;Spherical;World;False;Top Texture 0;_TopTexture0;white;-1;None;Mid Texture 0;_MidTexture0;white;-1;None;Bot Texture 0;_BotTexture0;white;-1;None;Triplanar Sampler;Tangent;10;0;SAMPLER2D;;False;5;FLOAT;1;False;1;SAMPLER2D;;False;6;FLOAT;0;False;2;SAMPLER2D;;False;7;FLOAT;0;False;9;FLOAT3;0,0,0;False;8;FLOAT;1;False;3;FLOAT2;1,1;False;4;FLOAT;1;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;70;-882.0283,-143.0679;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;11;383.5299,-371.2596;Inherit;True;Property;_Normal;Normal;5;0;Create;True;0;0;0;False;0;False;-1;None;2789cb570d3b5d644a18ae224425fc99;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldPosInputsNode;121;-1237.893,-1614.13;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;96;105.0801,-1778.67;Inherit;True;Property;_TextureSample0;Texture Sample 0;5;0;Create;True;0;0;0;False;0;False;-1;None;48f723e162dac5c45b654e83bf9dd3d5;True;0;False;white;Auto;False;Object;-1;MipLevel;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;83;1179.378,1042.583;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;1620.883,-59.69974;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;cfantauzzo/Mossy Forest/Leaves;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.5;True;True;0;False;TransparentCutout;;AlphaTest;ForwardOnly;14;d3d9;d3d11_9x;d3d11;glcore;gles;gles3;metal;vulkan;xbox360;xboxone;ps4;psp2;n3ds;wiiu;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;0;4;10;25;False;0.5;True;0;0;False;-1;0;False;-1;0;0;False;-1;0;False;-1;1;False;-1;1;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;6;14;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;115;0;114;0
WireConnection;115;1;113;0
WireConnection;108;0;104;0
WireConnection;117;0;108;0
WireConnection;117;1;115;0
WireConnection;102;0;101;0
WireConnection;102;1;117;0
WireConnection;106;0;102;0
WireConnection;106;1;105;0
WireConnection;120;0;119;1
WireConnection;120;1;119;0
WireConnection;97;0;103;0
WireConnection;97;1;106;0
WireConnection;118;0;103;0
WireConnection;118;1;97;0
WireConnection;118;2;120;0
WireConnection;109;0;118;0
WireConnection;25;0;24;0
WireConnection;25;1;26;0
WireConnection;12;1;110;0
WireConnection;16;0;27;0
WireConnection;16;1;25;0
WireConnection;62;0;7;0
WireConnection;62;1;63;0
WireConnection;79;0;77;0
WireConnection;79;1;78;0
WireConnection;84;0;88;0
WireConnection;84;1;90;0
WireConnection;13;0;12;0
WireConnection;13;1;39;0
WireConnection;5;0;101;0
WireConnection;5;1;16;0
WireConnection;58;0;53;0
WireConnection;58;1;57;0
WireConnection;47;0;13;0
WireConnection;47;1;44;0
WireConnection;91;0;84;0
WireConnection;91;3;93;1
WireConnection;91;4;93;2
WireConnection;14;1;110;0
WireConnection;80;0;79;0
WireConnection;64;0;58;0
WireConnection;64;1;62;0
WireConnection;8;0;5;0
WireConnection;8;1;15;0
WireConnection;38;0;31;1
WireConnection;38;1;31;0
WireConnection;87;0;66;0
WireConnection;87;1;86;0
WireConnection;87;2;91;0
WireConnection;73;0;47;0
WireConnection;28;1;111;0
WireConnection;17;0;14;4
WireConnection;17;1;18;0
WireConnection;76;0;74;0
WireConnection;81;0;80;0
WireConnection;6;0;8;0
WireConnection;6;1;64;0
WireConnection;2;0;95;0
WireConnection;2;1;109;0
WireConnection;72;0;28;0
WireConnection;72;1;71;0
WireConnection;65;0;2;0
WireConnection;65;1;87;0
WireConnection;32;0;6;0
WireConnection;32;1;38;0
WireConnection;122;0;101;0
WireConnection;70;0;17;0
WireConnection;70;1;73;0
WireConnection;11;1;112;0
WireConnection;11;5;69;0
WireConnection;96;0;95;0
WireConnection;96;1;109;0
WireConnection;96;2;94;0
WireConnection;83;0;74;0
WireConnection;83;1;76;0
WireConnection;83;2;81;0
WireConnection;0;0;65;0
WireConnection;0;1;11;0
WireConnection;0;4;70;0
WireConnection;0;5;73;0
WireConnection;0;7;72;0
WireConnection;0;10;96;4
WireConnection;0;11;32;0
WireConnection;0;12;83;0
ASEEND*/
//CHKSM=54FF0388CD4E508C56479A9F9021E53BAD1BC8EF