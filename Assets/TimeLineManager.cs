﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.Playables;

public class TimeLineManager : MonoBehaviour
{
    public List<GameObject> Timelines;
    //public List<Cinemachine.CinemachineVirtualCamera> cameras;

    
    // Start is called before the first frame update
    void Start()
    {
        Timelines[0].SetActive(true);
        Timelines[1].SetActive(false);
        Timelines[2].SetActive(false);
        Timelines[3].SetActive(false);
        Timelines[4].SetActive(false);
        Timelines[5].SetActive(false);
        Timelines[6].SetActive(false);
        Timelines[7].SetActive(false);
        Timelines[8].SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            Timelines[0].SetActive(false);
            Timelines[1].SetActive(true);
            Timelines[2].SetActive(false);
            Timelines[3].SetActive(false);
            Timelines[4].SetActive(false);
            Timelines[5].SetActive(false);
            Timelines[6].SetActive(false);
            Timelines[7].SetActive(false);
            Timelines[8].SetActive(false);
            Timelines[1].GetComponent<PlayableDirector>().Play();
        }
        else if (Input.GetKey(KeyCode.Alpha2))
        {
            Timelines[0].SetActive(false);
            Timelines[1].SetActive(false);
            Timelines[2].SetActive(true);
            Timelines[3].SetActive(false);
            Timelines[4].SetActive(false);
            Timelines[5].SetActive(false);
            Timelines[6].SetActive(false);
            Timelines[7].SetActive(false);
            Timelines[8].SetActive(false);
            Timelines[2].GetComponent<PlayableDirector>().Play();
        }
        else if (Input.GetKey(KeyCode.Alpha3))
        {
            Timelines[0].SetActive(false);
            Timelines[1].SetActive(false);
            Timelines[2].SetActive(false);
            Timelines[3].SetActive(true);
            Timelines[4].SetActive(false);
            Timelines[5].SetActive(false);
            Timelines[6].SetActive(false);
            Timelines[7].SetActive(false);
            Timelines[8].SetActive(false);
            Timelines[3].GetComponent<PlayableDirector>().Play();

        }
        else if (Input.GetKey(KeyCode.Alpha4))
        {
            Timelines[0].SetActive(false);
            Timelines[1].SetActive(false);
            Timelines[2].SetActive(false);
            Timelines[3].SetActive(false);
            Timelines[5].SetActive(false);
            Timelines[6].SetActive(false);
            Timelines[4].SetActive(true);
            Timelines[7].SetActive(false);
            Timelines[8].SetActive(false);
            Timelines[4].GetComponent<PlayableDirector>().Play();

        }else if (Input.GetKey(KeyCode.Alpha5))
        {
            Timelines[0].SetActive(true);
            Timelines[1].SetActive(false);
            Timelines[2].SetActive(false);
            Timelines[3].SetActive(false);
            Timelines[4].SetActive(false);
            Timelines[5].SetActive(false);
            Timelines[6].SetActive(false);
            Timelines[7].SetActive(false);
            Timelines[8].SetActive(false);
            Timelines[0].GetComponent<PlayableDirector>().Play();

        }else if (Input.GetKey(KeyCode.Alpha6))
        {
            Timelines[0].SetActive(false);
            Timelines[1].SetActive(false);
            Timelines[2].SetActive(false);
            Timelines[3].SetActive(false);
            Timelines[4].SetActive(false);
            Timelines[6].SetActive(false);
            Timelines[5].SetActive(true);
            Timelines[7].SetActive(false);
            Timelines[8].SetActive(false);
            Timelines[5].GetComponent<PlayableDirector>().Play();

        }else if (Input.GetKey(KeyCode.Alpha7))
        {
            Timelines[0].SetActive(false);
            Timelines[1].SetActive(false);
            Timelines[2].SetActive(false);
            Timelines[3].SetActive(false);
            Timelines[4].SetActive(false);
            Timelines[5].SetActive(false);
            Timelines[6].SetActive(true);
            Timelines[7].SetActive(false);
            Timelines[8].SetActive(false);
            Timelines[6].GetComponent<PlayableDirector>().Play();

        }else if (Input.GetKey(KeyCode.Alpha8))
        {
            Timelines[0].SetActive(false);
            Timelines[1].SetActive(false);
            Timelines[2].SetActive(false);
            Timelines[3].SetActive(false);
            Timelines[4].SetActive(false);
            Timelines[5].SetActive(false);
            Timelines[6].SetActive(false);
            Timelines[7].SetActive(true);
            Timelines[8].SetActive(false);
            Timelines[7].GetComponent<PlayableDirector>().Play();

        }else if (Input.GetKey(KeyCode.Alpha9))
        {
            Timelines[0].SetActive(false);
            Timelines[1].SetActive(false);
            Timelines[2].SetActive(false);
            Timelines[3].SetActive(false);
            Timelines[4].SetActive(false);
            Timelines[5].SetActive(false);
            Timelines[6].SetActive(false);
            Timelines[7].SetActive(false);
            Timelines[8].SetActive(true);
            Timelines[8].GetComponent<PlayableDirector>().Play();

        }
    }
}
