﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cloudsText : MonoBehaviour
{
    public GameObject text;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(WaitToOff());
    }

    // Update is called once per frame
 
    IEnumerator WaitToOff()
    {
        yield return new WaitForSecondsRealtime(3f);
        text.SetActive(false);
    }
   
    
}
