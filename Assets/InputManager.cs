﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class InputManager : Singleton<InputManager>
{
    public KeyCode slowMo = KeyCode.Mouse0;
    public KeyCode speedUp = KeyCode.D;
    public KeyCode speedDown = KeyCode.A;
    public KeyCode cam0 = KeyCode.I;
    public KeyCode cam1 = KeyCode.O;
    public KeyCode cam2 = KeyCode.P;
    public KeyCode generalCam = KeyCode.RightArrow;
    public KeyCode flickr = KeyCode.RightControl;
    public KeyCode changeScene = KeyCode.RightAlt;
    public KeyCode prevScene = KeyCode.LeftArrow;
    public KeyCode enableSwarm = KeyCode.Z;
    public KeyCode disableSwarm = KeyCode.X;
    public KeyCode disableArduino = KeyCode.V;
    public KeyCode enableArduino = KeyCode.B;
    DuinoManager serialRef;
    List<SwarmRef> tempSwarmRefs = new List<SwarmRef>();
    public FlockControllerRef flockRef;

    void Start()
    {
        flockRef = FlockControllerRef.main;
    }
    private void Update()
    {
        if (Input.GetKeyDown(enableSwarm))
        {
            tempSwarmRefs.ForEach(reference => reference.gameObject.SetActive(true));
            if (flockRef != null)
            {
                flockRef.gameObject.SetActive(true);
            }
        }
        if (Input.GetKeyDown(disableSwarm))
        {
            if (FindObjectsOfType<SwarmRef>().ToList().Count > 0)
                tempSwarmRefs = FindObjectsOfType<SwarmRef>().ToList();
            tempSwarmRefs.ForEach(reference => reference.gameObject.SetActive(false));
            flockRef = FlockControllerRef.main;
            if (flockRef != null)
            {
                flockRef.gameObject.SetActive(false);
            }
        }
        if (Input.GetKeyDown(disableArduino))
        {
            serialRef = FindObjectOfType<DuinoManager>();
            serialRef.gameObject.SetActive(false);
        }
        if (Input.GetKeyDown(enableArduino))
        {
            if (serialRef != null)
                serialRef.gameObject.SetActive(true);
        }
    }
}
