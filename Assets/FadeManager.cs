﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeManager : Singleton<FadeManager>
{
    public Image fadeImage;
    bool changingColor = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Fade(float _duration, Color _from, Color _to, ScreenFade callbackRef)
    {
        StartCoroutine(lerpColor(fadeImage, _from, _to, _duration, callbackRef));
    }

    public void StartFade(float _duration, Color _from, Color _to)
    {
        StartCoroutine(lerpColor(fadeImage, _from, _to, _duration));
    }
    IEnumerator lerpColor(Image targetImage, Color fromColor, Color toColor, float duration, ScreenFade callbackRef)
    {
        if (changingColor)
        {
            yield break;
        }
        changingColor = true;
        float counter = 0;

        while (counter < duration)
        {
            counter += Time.deltaTime;

            float colorTime = counter / duration;
            Debug.Log(colorTime);

            //Change color
            targetImage.color = Color.Lerp(fromColor, toColor, counter / duration);
            //Wait for a frame
            yield return null;
        }
        callbackRef.CallbackFade();
        targetImage.color = toColor;
       changingColor = false;
    }

    IEnumerator lerpColor(Image targetImage, Color fromColor, Color toColor, float duration)
    {
        if (changingColor)
        {
            yield break;
        }
        changingColor = true;
        float counter = 0;

        while (counter < duration)
        {
            counter += Time.deltaTime;

            float colorTime = counter / duration;
            Debug.Log(colorTime);

            //Change color
            targetImage.color = Color.Lerp(fromColor, toColor, counter / duration);
            //Wait for a frame
            yield return null;
        }
        targetImage.color = toColor;
        changingColor = false;
    }
}
