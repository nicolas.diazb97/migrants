﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
namespace Kvant
{
    public class FlockTrailController1 : Singleton<FlockTrailController>
    {
        public Swarm swarm;
        public FlockController1 flockController;
        public List<FlockChild1> birds;
        public CinemachineVirtualCamera virtualCam;
        public InputField min;
        public InputField max;
        float currSpeed = 5f;
        public bool controlSpeed = true;
        public bool onCave = false;
        // Start is called before the first frame update
        public void Init()
        {
            //StartCoroutine(ChangeTrailFocus());
            birds = flockController.gameObject.GetComponentsInChildren<FlockChild1>().ToList();
            int tempBird = Random.Range(0, birds.Count);
            virtualCam.Follow = birds[tempBird].transform;
            if (onCave)
                virtualCam.LookAt = birds[tempBird].transform;

        }

        private void Start()
        {
        }
        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                Init();
            }
            //flockController._maxSpeed = float.Parse(max.text);
            if (Input.GetKey(KeyCode.D))
            {
                currSpeed += 0.1f;
            }
            if (Input.GetKey(KeyCode.A))
            {
                currSpeed -= 0.1f;

            }
            SetSpeed();

        }
        void SetSpeed()
        {
            if (controlSpeed)
            {
                if (currSpeed > 1)
                {
                    float rand = Random.Range(currSpeed - 2, currSpeed);
                    flockController._minSpeed = (rand <= 1) ? 1 : rand;
                    flockController._maxSpeed = currSpeed;

                }
                else
                {
                    currSpeed = 1;
                    flockController._minSpeed = 1;
                }
            }
        }
        IEnumerator ChangeTrailFocus()
        {
            Debug.Log("chamge traoil focus");
            birds = flockController.gameObject.GetComponentsInChildren<FlockChild1>().ToList();
            swarm.attractor = birds[Random.Range(0, birds.Count)].transform;
            Debug.Log("se asigno attractor");
            yield return new WaitForSecondsRealtime(0.2f);
            StartCoroutine(ChangeTrailFocus());
        }
    }
}
