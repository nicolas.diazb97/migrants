﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SensorSetup : MonoBehaviour
{
    public Transform cam;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            SocketTest.main.fromX = cam.transform.rotation.x;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            SocketTest.main.fromY = cam.transform.rotation.y;
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            SocketTest.main.fromZ = cam.transform.rotation.z;
        }


        if (Input.GetKeyDown(KeyCode.F))
        {
            SocketTest.main.toX = cam.transform.rotation.x;
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            SocketTest.main.toY = cam.transform.rotation.y;
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            SocketTest.main.toZ = cam.transform.rotation.z;
        }
    }
}
