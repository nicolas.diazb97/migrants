﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Mewlist
{
    public class cumulonimbus : MonoBehaviour
    {
        [SerializeField] private MassiveCloudsProfile profile;
        [SerializeField] private MassiveClouds massiveClouds;
        [SerializeField] private Text label;

        public void SetProfile(MassiveCloudsProfile profile)
        {
            this.profile = profile;
            label.text = profile.name;
        }

        private void Start()
        {
            StartCoroutine(WaitForSetup());
        }

        IEnumerator WaitForSetup()
        {
            yield return new WaitForSecondsRealtime(2f);
            Switch();
        }
        //private void OnEnable()
        //{
        //    Toggle.onValueChanged.AddListener(Switch);
        //}

        //private void OnDisable()
        //{
        //    Toggle.onValueChanged.RemoveListener(Switch);
        //}

        // Use this for initialization
        public void Switch()
        {
            if (massiveClouds != null)
            {
                massiveClouds.SetProfiles(new List<MassiveCloudsProfile>() { profile });
            }
        }
    }
}