﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultipleCamController : MonoBehaviour
{
    public List<Camera> cams;
    KeyCode cam0;
    KeyCode cam1;
    KeyCode cam2;
    // Start is called before the first frame update
    void Start()
    {
        cam0 = InputManager.main.cam0;
        cam1 = InputManager.main.cam1;
        cam2 = InputManager.main.cam2;
        SetMainCamera(cams[0]);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(cam2))
        {
            SetCamViewport(1, -0.5f, 0);
            SetCamViewport(2, 0.5f, 0);
            SetCamViewport(0, 0, -0.5f);
            FirstCamFix();
        }
        if (Input.GetKeyDown(cam1))
        {
            SetCamViewport(0, 0.5f, 0);
            SetCamViewport(1, -0.5f, 0);
            SetCamViewport(2, 0, 0);
            FirstCamFix();
        }
        if (Input.GetKeyDown(cam0))
        {
            SetCamViewport(0, 0, 0);
            SetCamViewport(1, 0, 0);
            SetCamViewport(2, 0, 0);
            SetMainCamera(cams[0]);
            FirstCamFix();
        }
    }
    public void SetCamViewport(int camIndex, float viewportX, float viewportY)
    {
        cams.ForEach(c => c.gameObject.SetActive(true));
        cams[camIndex].rect = new Rect(viewportX, viewportY, cams[camIndex].rect.width, cams[camIndex].rect.height);
    }
    public void SetMainCamera(Camera mainCam)
    {
        cams.ForEach(c =>
        {
            if (c != mainCam)
            {
                c.gameObject.SetActive(false);
            }
        });
    }
    public void FirstCamFix()
    {
        cams[0].gameObject.SetActive(false);
        cams[0].gameObject.SetActive(true);
    }
}
